class Maillon:
    """Un maillon est donné par son élément et son maillon suivant à droite,
    éventuellement None.
    """

    def __init__(self, élément, droite):
        self.élément = élément
        self.droite = droite

    def __str__(self):
        return str(self.élément)


class Liste:
    """Une liste est donnée par son maillon de gauche
    ajouter/extraire à gauche correspond à empiler/dépiler
    """

    def __init__(self):
        self.maillon_gauche = None
    
    def est_vide(self):
        return self.maillon_gauche is None
    
    def ajout_gauche(self, élément):
        self.maillon_gauche = Maillon(élément, self.maillon_gauche)

    def extrait_gauche(self):
        if self.est_vide():
            raise ValueError("Liste vide")
        élément = self.maillon_gauche.élément
        self.maillon_gauche = self.maillon_gauche.droite
        return élément
    
    def __str__(self):
        affichage = "Contenu : "
        maillon = self.maillon_gauche
        while maillon is not None:
            affichage += str(maillon) + "::"
            maillon = maillon.droite
        affichage += " fin."
        return affichage
    
if __name__ == '__main__':
    # tests
    test = Liste()

    # test est_vide
    assert test.est_vide()

    # test un ajout_droite ; extraction_gauche
    test.ajout_gauche(42)
    élément = test.extrait_gauche()
    assert élément == 42
    assert test.est_vide()

    # test plusieurs ajouts
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.ajout_gauche(élément)
    assert [test.extrait_gauche()
            for _ in range(len(premiers))] == premiers[::-1]
    assert test.est_vide()
    
