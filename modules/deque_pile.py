class Pile():
    """Classe pile basée sur les listes dynamiques de Python
    """
    def __init__(self):
        self.données = []

    def __str__(self) -> str:
        return str(self.données)

    def est_vide(self) -> bool:
        return self.données == []

    def empile(self, valeur):
        self.données.append(valeur)
    
    def dépile(self):
        if self.est_vide():
            raise ValueError('Pile vide')
        return self.données.pop()

class Deque:
    """Classe deque basée sur deux piles
    """

    def __init__(self):
        self.pile_gauche = Pile()
        self.pile_droite = Pile()
    
    def est_vide(self):
        return self.pile_gauche.est_vide() and self.pile_droite.est_vide()
    
    def ajout_gauche(self, élément):
        self.pile_gauche.empile(élément)

    def ajout_droite(self, élément):
        self.pile_droite.empile(élément)

    def extrait_gauche(self):
        if self.est_vide():
            raise ValueError("Deque vide")
        if self.pile_gauche.est_vide():
            while not self.pile_droite.est_vide():
                self.pile_gauche.empile(self.pile_droite.dépile())
        élément_gauche = self.pile_gauche.dépile()
        return élément_gauche

    def extrait_droite(self):
        if self.est_vide():
            raise ValueError("Deque vide")
        if self.pile_droite.est_vide():
            while not self.pile_gauche.est_vide():
                self.pile_droite.empile(self.pile_gauche.dépile())
        élément_droite = self.pile_droite.dépile()
        return élément_droite

if __name__ == '__main__':
    # tests
    test = Deque()

    # test est_vide
    assert test.est_vide()

    # test un ajout/extraction à droite
    test.ajout_droite(42)
    élément = test.extrait_droite()
    assert élément == 42
    assert test.est_vide()

    # test un ajout/extraction à gauche
    test.ajout_gauche(1337)
    élément = test.extrait_gauche()
    assert élément == 1337
    assert test.est_vide()

    # test plusieurs ajouts
    premiers = [2, 3, 5, 7, 11]

    for élément in premiers:
        test.ajout_gauche(élément)
    assert [test.extrait_droite()
            for _ in range(len(premiers))] == premiers
    assert test.est_vide()
    
    for élément in premiers:
        test.ajout_droite(élément)
    assert [test.extrait_gauche()
            for _ in range(len(premiers))] == premiers
    assert test.est_vide()
    
