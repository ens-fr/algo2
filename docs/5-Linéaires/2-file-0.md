# ⛽La file, 🚦la deque

## La file

* Le principe de la **pile** est : *LIFO : Last In, First Out*, (dernier entré, premier sorti).
* Le principe de la **file** est : *FIFO : First In, First Out*, (premier entré, premier sorti).

![file](assets/FIFO_PEPS.png)

> Wikipedia [file](https://fr.wikipedia.org/wiki/File_(structure_de_donn%C3%A9es))

### Utilisations concrètes

* Les travaux à imprimer sont envoyés dans une file, le premier arrivé sera le premier servi.
* Un processeur reçoit des calculs à effectuer, ils arrivent dans une file. L'ordonnanceur est souvent plus élaboré qu'une simple file et des travaux prioritaires peuvent être traités avant.

!!! faq "Exercice 1"
    En s'inspirant de la **première** implémentation de la pile (avec un tableau), donner une implémentation d'une **file** d'une certaine taille maximale. On proposera le constructeur ainsi que les méthodes `.est_vide(self)`, `.enfile(self, élément)` et `.défile(self)` analogues au cas de la pile.

!!! faq "Exercice 2"
    En utilisant une implémentation de la pile, donner une implémentation de la file. On utilisera deux piles.

    On pourra s'inspirer d'une situation de jeux de cartes avec deux piles : la pioche et la défausse. Quand la pioche est vide, on retourne la défausse qui devient la pioche.

!!! faq "Exercice 3"
    Résoudre le problème [Distributeur automatique](http://www.france-ioi.org/algo/task.php?idChapter=527&iOrder=2) sur France-IOI.

    ??? done "Réponse"
        À étudier **après** avoir vu la deque !
        [Réponse détaillée](https://ens-fr.gitlab.io/france-ioi/N3/07-SDE/3-distri_auto/)

!!! tip "Aide"
    On pourra considérer [ce devoir](TAD-file-eval.pdf) et ses indications.

    **Conseil** : on peut résoudre les problèmes dans un premier temps sans l'écriture avec style POO. Cependant, on demande alors une seconde écriture. Pourquoi ?

    * Le jour où on dispose d'une meilleure structure de données, il suffit de remplacer uniquement le bout de code de la classe, le problème restant intact. Sans POO, il faut souvent réécrire tout le problème pour utiliser les nouvelles idées... L'écriture avec le style POO permet de s'affranchir presque totalement de la manière dont est écrit la classe. Il faut en revanche **toujours** garder à l'esprit : quel est le cout algorithmique de chaque méthode ?

!!! info "Toujours utile"
    Lire plusieurs fois [le tutoriel sur les structures de données sur python.org](https://docs.python.org/fr/3/tutorial/datastructures.html)


## La deque

En plus des structures [présentées ici](https://fr.wikipedia.org/wiki/Type_abstrait), il existe une autre structure linéaire assez utilisée.

* La `deque` : (*double end queue*), une structure qui permet facilement d'ajouter ou d'extraire facilement un élément à une des deux extrémités, si elle est non vide.

!!! info "Deque avec liste dynamique"

    On utilise les facilités du langage Python (avec les listes dynamiques) pour construire la `#!python class Deque`. _On triche._

    ```python
    --8<-- "modules/deque_dyn.py"
    ```

    Cette méthode n'est pas efficace pour l'ajout et l'extraction à gauche !

    - En effet, `pop(0)` oblige toute la liste à se décaler d'un rang à gauche.
    - Idem pour une insertion à gauche.

!!! tip "Deque du module `collections`"
    On utilise encore plus de facilités du langage Python, la structure de données *Deque* est incluse dans `collections`.

    ```python
    --8<-- "modules/deque_collections.py"
    ```

    [Lire la documentation Python de la deque](https://docs.python.org/fr/3/library/collections.html#collections.deque)

    Cette méthode est efficace, c'est celle qu'il faut utiliser avec le langage Python en mode de production ; cependant **elle n'est pas pédagogique**, on _triche trop_. Tentons une façon de construire une implémentation efficace.


!!! faq "Exercice 1 : Deque avec un tableau"
    En s'inspirant de l'implémentation de la pile avec le tableau, donner une implémentation d'une **deque** d'une certaine taille maximale. On proposera :

    * la construction de la classe `Deque()`, ainsi que les méthodes,
    * `.est_vide(self)`
    * `.ajout_droite(self, élément)`
    * `.ajout_gauche(self, élément)`
    * `.extrait_droite(self)`
    * `.extrait_gauche(self)`

    ??? done "Réponse"

        ```python
        --8<-- "modules/deque_tableau.py"
        ```

!!! faq "Exercice 2 : Deque à deux piles"
    En utilisant **uniquement** une implémentation de la pile, donner une implémentation de la deque. On utilisera deux piles. Tout comme pour la file, critiquer la complexité des méthodes.

    ??? done "Réponse"
        Comme pour la file implémentée avec deux piles, on peut aussi implémenter la deque avec deux piles.

        ```python
        --8<-- "modules/deque_pile.py"
        ```
        
        Cette construction est **efficace sauf** dans le cas où une des deux piles est vide et alternativement on extrait un élément à gauche puis à droite. Dans ce cas, les piles sont alternativement retournées... **Très mauvais**.

        Pour une construction réellement efficace, on utilisera des listes doublement chainées.
