# 🚇La liste chaînée

## Le maillon

Dans une tentative de définition récursive de la pile, en Python, on a vu l'intérêt d'avoir un objet qui regroupe un élément et un lien vers la suite de la pile. Nous avions utilisé un tuple `(tête, queue)`, où `tête` était l'élément au sommet, et `queue` la suite des données de la pile, mais nous n'avions pas pu donner le status de pile à la `queue`. En utilisant deux classes, nous y arriverons.

Un maillon possède deux attributs, un élément et un **lien** vers le suivant.

```python
class Maillon:
    """Un maillon est donné par son élément et son maillon suivant à droite,
    éventuellement None.
    """

    def __init__(self, élément, droite):
        self.élément = élément
        self.droite = droite

    def __str__(self):
        return str(self.élément)
```

Voici un exemple d'utilisation simple.

```pycon
>>> m_1 = Maillon(42, None)
>>> m_2 = Maillon(1337, m_1)
>>> m_3 = Maillon(2021, m_2)
```

Et le schéma associé :

$$\boxed{m_3:(2021, \text{vers }m_2)} \rightarrow \boxed{m_2:(1337, \text{vers }m_1)} \rightarrow \boxed{m_1:(42, \text{vers le vide})} \rightarrow \text{Nil}$$

> **Nil** représente le vide.

Avec Python, le passage de paramètres se fait par un lien, l'adresse mémoire, ainsi les objets `m_1` et `m_2` ne sont pas copiés dans `m_2` et `m_3`, seule leur adresse est donnée.

!!! info "Le ramasse miette"
    Quand un objet n'est référencé nulle part, par aucune variable, qu'il ne peut donc plus être joignable, Python possède un mécanisme (le ramasse miette ; _garbage collector_) qui supprime les données inutilisées ; ainsi on pourra supprimer un maillon simplement en n'ayant plus aucune variable qui le référence.

On devine qu'on va pouvoir avec cette structure construire les méthodes pour une pile, mais aussi pour une file, pour une deque, et plus encore. On va pouvoir supprimer un maillon au milieu d'une chaine en faisant pointer le précédent sur un autre maillon, et pour insérer un maillon, il suffira de modifier les suivants de deux maillons...

## Listes chainées à un maillon

On va utiliser deux classes :

* Une classe `Maillon` qui contient un élément et le suivant dans la liste.
* Une classe `Liste` qui contient un maillon de tête et les méthodes pour la faire vivre.

On commence par construire les méthodes analogues à une pile, ensuite on complètera en deque.

### Première approche, implémenter la pile

```python
--8<-- "modules/liste0.py"
```

!!! faq "Exercice 1"
    Tester aussi cette implémentation de la pile. Sur FranceIOI par exemple.

### Implémenter la deque

Implémenter la deque est de difficulté comparable à implémenter la file, autant faire mieux immédiatement, on aura indirectement la file.

!!! faq "Exercice 2"
    Compléter la classe pour obtenir les méthodes de la deque. Ces méthodes sont-elles toutes efficaces ? Tester sur FranceIOI par exemple.

    ??? done "Réponse"
        ```python
        --8<-- "modules/liste1.py"
        ```

        !!! danger "Complexité"
            On pose $n$ la longueur de la chaine. La complexité des méthodes est la suivante :

            * `ajout_gauche` : $\mathcal O(1)$ ; il suffit de modifier un maillon, et d'en ajouter un. Coût constant.
            * `extrait_gauche` : $\mathcal O(1)$ ; il suffit de modifier un maillon. Coût constant.
            
            En revanche pour lire ou écrire à droite, il faut parcourir toute la chaine pour accéder à l'autre bout. Ce parcours a un coût linéaire en $n$.

            * `ajout_droite` : $\mathcal O(n)$ ; il suffit de modifier un maillon, et d'en ajouter un ; mais avant cela, il faut parcourir la liste entièrement.
            * `extrait_droite` : $\mathcal O(n)$.

        !!! info "Première conclusion"
            Si on a besoin uniquement d'une structure de pile, alors la liste simplement chaînée à un maillon est bien adaptée.
            
            **Avantages** :
            
            - Il n'y pas de limite fixée à l'avance pour la taille de la pile.
            - C'est un peu plus technique à écrire ; donc pédagogique...

            **Inconvénients** :
            
            - Utilise un peu plus de mémoire, surtout si les éléments de données sont de petite taille ; dans ce cas l'empreinte mémoire est doublée. Si chaque élément de donnée est imposant, alors c'est peu sensible. Cela fournit donc une implémentation d'autant plus lente.
            - C'est un peu plus technique à écrire ; mais pédagogique...

        Pour bénéficier d'une structure **efficace** de file (ou de deque, pour laquelle on a presque le même coût intellectuel de construction), on va utiliser :

        - des listes qui mémorisent le maillon gauche et droite,
        - et/ou des maillons qui pointent à droite et à gauche.


## Listes chainées à deux maillons

### Implémenter la file

On garde le **même** maillon, mais la liste possède deux attributs : `maillon_gauche` et `maillon_droite`.

!!! faq "Exercice 3"
    Écrire une classe `Liste` avec uniquement les méthodes d'une file.

    ??? done "Réponse"
        On peut avoir une file en conservant correctement le maillon gauche et le maillon droite de la liste.

        ```python
        --8<-- "modules/file.py"
        ```

        !!! danger "Complexité"
            On pose $n$ la longueur de la chaine. La complexité des méthodes est la suivante :

            * `ajout_gauche` : $\mathcal O(1)$ ; il suffit de modifier un maillon, et d'en ajouter un. Coût constant.
            * `extrait_droite` : $\mathcal O(1)$ ; il suffit de modifier un maillon. Coût constant.

        !!! info "Conclusion"
            Si on a besoin de construire une classe file, cette liste simplement chaînée à deux maillons est intéressante.


## Listes doublement chainées

On utilise un nouveau type de maillon, qui possède trois attributs :

* son élément de donnée,
* un lien vers la gauche,
* un lien vers la droite.

### Implémenter la deque

!!! faq "Exercice 4"
    Réécrire une implémentation de la deque avec une liste doublement chainée. Les méthodes sont-elles toutes efficaces ? Tester sur FranceIOI par exemple.
    Les parties gauche et droite sont totalement symétriques, il suffit de faire un copié/collé et d'échanger droite et gauche.

    ??? done "Réponse"
        On utilise des maillons qui pointent dans les deux sens.

        On donne une implémentation pour une deque en conservant les deux maillons gauche et droite.

        ```python
        --8<-- "modules/liste2.py"
        ```

        Pour tester avec France-IOI, on pourra essayer les problèmes

        * [Dates de péremption](https://ens-fr.gitlab.io/france-ioi/N3/07-SDE/2-dates_p%C3%A9remption/) ; pour la pile.
        * [Distributeur automatique](https://ens-fr.gitlab.io/france-ioi/N3/07-SDE/3-distri_auto/) ; pour la file et la deque.

        Pour lesquels vous avez déjà un corrigé avec plusieurs implémentations comparées de pile, de file ou de deque.

### Illustration des méthodes

#### `ajout_gauche`

##### Cas 1, si la liste est vide
Si la liste est vide, on crée un maillon qui sera le maillon gauche et aussi droite de la liste, il ne pointe nulle part, ni à droite, ni à gauche.

Exemple où on ajoute un premier maillon avec l'élément $42$.

$$\text{Nil} \leftarrow \boxed{\text{vers le vide} ; 42 ; \text{vers le vide}} \rightarrow \text{Nil}$$

##### Cas 2, si la liste est non vide

Si la liste est non vide, alors :

* On crée un nouveau maillon, et ce maillon sera le nouveau maillon gauche de la liste.
* À sa droite se trouvera l'ancien maillon gauche de la liste.
* Cet ancien maillon gauche pointait avant à gauche sur le vide, désormais il pointera sur le nouveau maillon.

Exemple où le maillon gauche possède l'attribut élément à $42$. On ajoute à gauche un maillon avec $1337$.

* Avant : $\qquad\text{Nil} \leftarrow m_1: \boxed{\text{vers le vide} ; 42 ; \text{vers la suite}} \rightarrow ...$
    * On crée le maillon $m_0$ :
        * qui pointe à gauche sur le vide,
        * avec l'élément $1337$,
        * qui pointe à droite sur $m_1$.
    * On modifie $m_1$, il pointera désormais à gauche vers $m_0$.

* Après : $\qquad\text{Nil} \leftarrow m_0: \boxed{\text{vers le vide} ; 1337 ; \text{vers }m_1} \leftrightarrow m_1: \boxed{\text{vers }m_0 ; 42 ; \text{vers la suite}} \rightarrow ...$

#### `extrait_gauche`

##### Cas 1, la liste est vide

On ne peut pas extraire, on lève une exception.

##### Cas 2, la liste ne contient qu'un élément

Dans ce cas, le maillon de gauche existe, mais il pointe à sa droite sur le vide. En supprimant un maillon, la liste devient vide.

##### Cas 3, la liste contient plusieurs éléments

Dans ce cas, le maillon de gauche existe et il pointe à sa droite sur un autre maillon ; ce maillon sera le nouveau maillon gauche.

Exemple où le maillon gauche possède l'attribut élément à $1337$, et pointe vers un maillon avec $42$.


* Avant : $\qquad\text{Nil} \leftarrow m_0: \boxed{\text{vers le vide} ; 1337 ; \text{vers }m_1} \leftrightarrow m_1: \boxed{\text{vers }m_0 ; 42 ; \text{vers la suite}} \rightarrow ...$
    * On enregistre la valeur $1337$, pour la renvoyer à la fin.
    * On établit que le maillon gauche de la liste est celui à droite de l'ancien maillon gauche. Ici, $m_1$ devient le maillon gauche de la liste.
    * On modifie $m_1$ afin qu'il pointe désormais à gauche sur le vide.
    * On renvoie $1337$.
* Après : $\qquad\text{Nil} \leftarrow m_1: \boxed{\text{vers le vide} ; 42 ; \text{vers la suite}} \rightarrow ...$

!!! danger "Complexité"
    On pose $n$ la longueur de la chaine. La complexité des méthodes est la suivante :

    * `ajout_gauche` : $\mathcal O(1)$ ; il suffit de modifier un maillon, et d'en ajouter un. Coût constant.
    * `extrait_gauche` : $\mathcal O(1)$ ; il suffit de modifier un maillon. Coût constant.
    * `ajout_droite` : $\mathcal O(1)$ ; il suffit de modifier un maillon, et d'en ajouter un. Coût constant.
    * `extrait_droite` : $\mathcal O(1)$ ; il suffit de modifier un maillon. Coût constant.

!!! info "Conclusion"
    Si on a besoin de construire une deque, la liste doublement chainée est intéressante, surtout si les éléments sont volumineux, mais peu nombreux. **Cette construction est aussi pédagogique.**

Avec Python, en dehors de l'aspect pédagogique, on utilisera la deque fournie dans `collections`.

## Et ensuite

Donnons maintenant une motivation à vouloir créer de nouvelles structures de données toujours plus efficaces.

* On aimerait pouvoir insérer ou supprimer ailleurs qu'aux extrémités.
    * On devine qu'avec les maillons, il est facile d'insérer une valeur au milieu ou ailleurs dans une liste, il suffira de modifier quelques liens droite et gauche des maillons concernés. Mais pour accéder à ce maillon, il faut le rechercher...
* Pour chercher un élément, il faut parcourir la liste, et dans le pire des cas, **toute la liste**.
    * On ne peut pas non plus avoir la notion d'indice comme dans un tableau. En effet, mettre à jour les indices à chaque ajout/suppression donne une mauvaise complexité ; il faudra parfois parcourir toute la liste.

!!! faq "Une structure pour chercher-insérer-supprimer"
    On aimerait une structure de données, où :

    * La recherche d'un élément est aussi rapide que possible.
    * L'insertion d'un élément est aussi rapide que possible.
    * La suppression d'un élément est aussi rapide que possible.

    Les **a**rbres **b**inaires de **r**echerche (ABR) seront un début de réponse à cette question.
