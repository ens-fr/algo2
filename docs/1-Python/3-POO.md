# 🛰️ POO

Programmation Orientée Objet (POO)

## Introduction

Construisons par exemple une façon de travailler avec les fractions.

```python
# ancienne méthode pour travailler avec des fractions
frac1_numérateur = 5
frac1_dénominateur = 7
frac2_numérateur = 13
frac2_dénominateur = 8
```

Cette façon n'est pas pratique. La gestion des identifiants s'avère pénible dès que les objets sont complexes.
En particulier pour les fonctions et leurs paramètres ; comparons :

1. `frac3_numérateur, frac3_dénominateur = addition(frac1_numérateur, frac1_dénominateur, frac2_numérateur, frac2_dénominateur)`
2. `frac3 = frac1.addition(frac2)`

La seconde est clairement plus lisible, il s'agit de POO.

- L'expression `frac1.addition(frac2)` possède un objet `frac1` qui possède une méthode `addition` qui peut réaliser une action au nom de `frac1`, ou pour lui, ou dont il est le sujet... Cette méthode prend, ici, un paramètre `frac2`...

## Définition de nouvelles classes d'objets

Le choix d'un identifiant de classe en Python se fait en utilisant le *CamelCase*, la première lettre de chaque mot en majuscule, et pas de `_`.

### Première approche ; rudimentaire

Pour définir une classe `Fraction` :

```python
class Fraction:
    pass

f = Fraction()
f.numérateur = 5
f.dénominateur = 8
h = Fraction()
h.numérateur = 5
h.dénominateur = 8


g = f
f.numérateur = 11
print("g:", g.numérateur, "sur", g.dénominateur)
print("h:", h.numérateur, "sur", h.dénominateur)
```

```output
g: 11 sur 8
```

1. On constate que l'on peut travailler d'un coup avec tout l'objet. C'est pratique.
2. Ici, on n'a pas eu une copie indépendante, mais le **même objet** pointé par deux variables différentes. `g` et `f` sont **le même objet**.
3. `h` est un **autre** objet qui possédait les mêmes attributs que `f` au début...

> :warning: C'est mal de donner des attributs à un objet en dehors d'un constructeur. **Ne plus jamais refaire,** sauf cas bien précis.

### Deuxième approche ; attributs de classe

Les attributs ne sont définis que dans la classe :

```python
class Fraction:
    numérateur = 5
    dénominateur = 8

f = Fraction()
h = Fraction()

g = f
f.numérateur = 11
print("Fraction g :", g.numérateur, "sur", g.dénominateur)
print("Fraction h :", h.numérateur, "sur", h.dénominateur)
```

```output
Fraction g : 11 sur 8
Fraction h : 5 sur 8
```

### Troisième approche ; avec initialisation `__init__()`

On aimerait un constructeur pour créer une fraction qui dépend de paramètres.


```python
class Fraction:
    def __init__(self, a, b):
        self.numerateur = a
        self.denominateur = b


f = Fraction(22, 5)
f.denominateur = 7
print("Fraction f :", f.numerateur, "sur", f.denominateur)
```

```output
Fraction f : 22 sur 7
```

Il est possible d'accéder ou de modifier un attribut de type donnée d'un objet.

- Dans la classe, l'objet en question est `self`, son attribut est `self.son_attribut`
- Avec une utilisation concrète d'un tel `truc`, on y accède en lecture-écriture avec `truc.son_attribut`

### Quatrième approche ; encapsulation

Il est **parfois** mal de lire ou modifier les attributs directement ; on préfère passer **alors** par des fonctions. C'est un aspect de la modularité. On utilisera donc, dans ce cas, des méthodes pour lire, et d'autres pour modifier.


```python
class Fraction:
    def __init__(self, a, b):
        self.__numerateur = a
        self.__denominateur = b
    
    def donne_numerateur(self):
        return self.__numerateur

    def donne_denominateur(self):
        return self.__denominateur

    def modifie_numerateur(self, a):
        self.__numerateur = a

    def modifie_denominateur(self, b):
        self.__denominateur = b



f = Fraction(22, 5)
f.modifie_denominateur(7)
print("Fraction f :", f.donne_numerateur(), "sur", f.donne_denominateur())
```

```output
Fraction f : 22 sur 7
```


- Les attributs qui doivent être indiqués **privés** commencent par `__` (double *underscore*). Ce **n**'est **pas** qu'une indication en Python, et ils ne peuvent plus (facilement) être lus ni modifiés à l'extérieur de la définition de la classe. Dans d'autres langages de programmation, la gestion public-privé est encore plus stricte.

- Les méthodes liées au fonctionnement interne sont encadrées de `__` ; ici `__init__()` est la méthode à redéfinir pour initialiser un objet après son constructeur. **On ne peut pas changer de nom !** Il existe automatiquement le constructeur `__new__`, mais en pratique, on réécrit uniquement `__init__`.

- `self` indique l'objet même, instancié par la classe. Lui-même (*self*). **On ne peut pas changer ce nom !**

> ⚠️ On aimerait une méthode plus simple pour afficher le résultat. On va créer une méthode qui sera appelée par `print` ; une fonction de conversion vers le type `str` ; c'est la méthode `__str__()`. **On ne peut pas changer de nom !**
> On ajoute aussi une méthode `__repr__()` qui a pour rôle de fournir un affichage bien plus succinct, moins joli, mais pour le débogage. Elle est appelée par la fonction `repr`. C'est la méthode qui est utilisée en console lorsqu'on évalue une expression, **tout** résultat étant un objet. **En Python, tout est objet**.

```pycon
>>> a = 2.0   # un objet quelconque créé
>>> print(a)  # str sera appelé
2.0
>>> a         # repr sera appelé
2.0
```

Souvent `repr` et `str` sont identiques, mais ce n'est pas obligatoire. `repr` sera plus à destination d'un développeur et `str` pour l'utilisateur.

On ajoute dans la classe, juste après le `__init__()`

```python
    # suite de Fraction
    def __str__(self):
        return f"({f.__numérateur()}/{f.__dénominateur()})"

    def __repr__(self):
        return f"Fraction({f.__numérateur()}, {f.__dénominateur()})"
```

Et on teste
```pycon
>>> f = Fraction(22, 5)
>>> f.modifie_dénominateur(7)
>>> print(f)
(22/7)
>>> f
Fraction(22, 7)
```

C'est bien plus pratique à utiliser !

!!! info "Astuce"
    L'idée avec __repr__ c'est qu'on puisse reconstruire l'objet via `eval` par exemple.

:warning: Le code n'est pas encore satisfaisant, il manque toutes les *docstring*.

## Encapsulation

L'encapsulation est un des trois principes fondamentaux de la POO (avec l'héritage et le polymorphisme).

- On réunit avec une certaine unité les données et les méthodes ; avec toujours des fonctions.
- On *masque* à l'utilisateur externe les données ; il y accède uniquement via des méthodes qui contrôlent les données comme prévu en interne.
- Cela permet de pouvoir réécrire entièrement le moteur interne, de manière transparente pour l'utilisateur final, qui ne sait pas comment sont stockées les données *in fine*.

Il y a donc des méthodes particulières :

- **accesseur** (*getter*) ; renvoie un attribut
- **mutateur** (*setter*) ; modifie un attribut

> On trouve de **nombreuses** méthodes qui commencent par `get_` ou par `set_`, comme :

```python
class Personne:
    """Classe représentant une personne"""

    def __init__(self, nom: str, prenom: str, age: int):
        self.__nom = nom
        self.__prenom = prenom
        self.__age = age

    def get_name(self) -> str:
        return self.__nom

    def set_name(self, nom: str):
        self.__nom = nom

    # ... idem pour les deux autres
```

## Méthodes publiques

Ajoutons une méthode pour multiplier deux fractions.

```python
    # suite de Fraction
    def multiplie_par(self, autre):
        self.__numerateur *= autre.donne_numerateur()
        self.__denominateur *= autre.donne_denominateur()
```

```pycon
>>> f = Fraction(2, 3)
>>> g = Fraction(5, 7)
>>> f.multiplie_par(g)
>>> f
Fraction(10, 21)
```

:warning: On remarquera, que pour `self`, on peut travailler avec ses attributs privés, mais pour `autre`, nous avons utilisé les méthodes définies avant.

## Exercices

On reprend le code précédent complet :

```python
class Fraction:
    def __init__(self, a, b):
        self.__numerateur = a
        self.__denominateur = b

    def __str__(self):
        return f"({f.__numerateur()}/{f.__denominateur()})"

    def __repr__(self):
        return f"Fraction({f.__numerateur()}, {f.__denominateur()})"

    def donne_numerateur(self):
        return self.__numerateur

    def donne_denominateur(self):
        return self.__denominateur

    def modifie_numerateur(self, a):
        self.__numerateur = a

    def modifie_denominateur(self, b):
        self.__denominateur = b
    
    def multiplie_par(self, autre):
        self.__numerateur *= autre.donne_numerateur()
        self.__dénominateur *= autre.donne_denominateur()
```

```pycon
>>> f = Fraction(2, 3)
>>> g = Fraction(5, 7)
>>> f.multiplie_par(g)
>>> f
Fraction(10, 21)
```

1. Ajouter les *docstring*
2. Ajouter une méthode `simplifier()`
3. Ajouter une méthode `ajouter(autre)`

??? done "Réponse"

    ```python
    from math import gcd  # pour le PGCD de deux entiers

    class Fraction:

        # ... la suite

        def simplifier(self):
            """On simplifie la fraction self par
                  le PGCD de son numérateur et de son dénominateur.
            """
            a = self.__numerateur
            b = self.__denominateur
            k = gcd(a, b)
            self.__numerateur = a // k
            self.__denominateur = b // k
        
        def ajouter(self, autre):
            "On ajoute à self une autre fraction"

            # Méthode
            """
            self = a/b  ; autre = c/d
            Il faut les mettre au même dénominateur.
            On peut faire
                F = a/b + c/d
                F = (a*d)/(b*d) + (c*b)/(d*b)
                F = (a*d + c*b) / (b*d)
            Que l'on simplifiera ensuite
            """
            a = self.__numerateur
            b = self.__denominateur
            c = autre.donne_numerateur()
            d = autre.donne_denominateur()

            self.__numerateur = a*d + c*b
            self.__denominateur = b*d
            
            self.simplifier()
    ```


Pour la suite, on peut regarder :

- ce [cours](assets-POO/POO.pdf) avec des notions hors programme, pour aller plus loin.
- ce [cours](https://nbviewer.jupyter.org/url/www.maths-info-lycee.fr/notebooks/tnsi_01_poo.ipynb) illustré ; attention le PEP-8 n'est pas respecté, et il y a quelques erreurs glissées dans les images.
- Un **très joli** [cours](https://glassus.github.io/terminale_nsi/T2_Programmation/2.1_Programmation_Orientee_Objet/cours/) fait avec MkDocs !
