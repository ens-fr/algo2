# Sommaire

1. Travailler avec les fstring
2. Bien documenter son code
    - Commentaires
    - docstring
    - doctest
    - fonction help
4. POO
3. Gestion des erreurs
8. Mathématiques
