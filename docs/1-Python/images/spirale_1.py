from svg_turtle import SvgTurtle

t = SvgTurtle(250, 250)

t.pensize(3)


n = 10
rayon = 0
for k in range(n):
    rayon += 10         # le rayon augmente à chaque tour
    t.circle(rayon, 180)  # un demi-cercle
    t.stamp()

t.save_as('spirale_1.svg')
