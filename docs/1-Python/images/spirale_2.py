from svg_turtle import SvgTurtle

t = SvgTurtle(250, 250)

t.pensize(3)


n = 20
rayon = 0
for k in range(n):
    rayon += 5
    t.circle(rayon, 90)
    t.stamp()

t.save_as('spirale_2.svg')
