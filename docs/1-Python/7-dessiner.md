# 🖍️ Dessiner

Présentation de deux outils graphiques complémentaires pour Python

1. Turtle[^turtle] permet de tracer des figures, la trace qu'une tortue aurait pu laisser suite à des instructions comme « avance de 10 m, tourne à droite de 20°, avance de 5 m ...».
2. DrawSvg[^drawsvg] permet de construire des images vectorielles en ajoutant des formes géométriques élémentaires définies à partir de coordonnées.

[^turtle]: :material-language-python: [Documentation de la bibliothèque `turtle`](https://docs.python.org/fr/3/library/turtle.html) : Tortue graphique
[^drawsvg]: :material-github: [Dépôt et documentation de drawSvg](https://github.com/cduck/drawSvg) : génération d'images vectorielles avec Python


## Turtle

!!! info "Blockly Games"
    Avant Python, il est recommandé de commencer la tortue avec Blockly Games[^blockly].
    
    [Voici une version en local](./blockly-games/fr/turtle.html?lang=fr){ .md-button } ← cliquer ici !

    [^blockly]: Jeux pour les programmeurs de demain : [Blockly Games](https://blockly.games/?lang=fr)

### Fonctions élémentaires

Les coordonnées, les distances et les angles sont des nombres entiers ou flottants.

!!! info "Les plus importantes"

    - `forward(distance)` : Avance la tortue de la `distance` spécifiée, dans la direction où elle se dirige.
    - `backward(distance)` : Déplace la tortue de `distance` vers l'arrière (dans le sens opposé à celui vers lequel elle pointe). Ne change pas le cap de la tortue.
    - `right(angle)` : Tourne la tortue à droite de `angle` degrés.
    - `left(angle)` : Tourne la tortue à gauche de `angle` degrés.

!!! tip "Autres élémentaires"

    - `pendown()` : Baisse la pointe du stylo — dessine quand il se déplace.
    - `penup()` : Lève la pointe du stylo — pas de dessin quand il se déplace.
    - `pensize(epaisseur)` : Règle l'`epaisseur` de la ligne.
    - `position()` : Renvoie la position actuelle de la tortue `(x, y)`.
    - `goto(x, y)` ou `goto((x, y))` : Déplace la tortue vers une position absolue. Si le stylo est en bas, trace une ligne. Ne change pas l'orientation de la tortue.
    - `heading()` : Renvoie le cap de la tortue.
    - `setheading(angle)` : Règle le cap de la tortue à la valeur `angle`. `0` pour l'Est, `90` pour le Nord, `180` pour l'Ouest, `270` pour le Sud...
    - `speed(vitesse)` : Règle la vitesse de 1 (lente) à 11 (la plus rapide).
    - `stamp()` : Tamponne une copie de la forme de la tortue sur le canevas à la position actuelle de la tortue.

Dans la documentation, on pourra trouver quelques variations, inutiles pour commencer.

### Fonctions moins élémentaires

Il faudra lire la documentation attentivement.

- `circle` : pour tracer des approximations de cercles ou des polygones réguliers.
- `towards` : pour calculer un cap.
- `distance` : pour calculer une distance.
- `color` : pour ce qui concerne les couleurs.

Il y a encore d'autres fonctions...


### Exercice : Spirale

![](images/spirale_1.svg){ .autolight }

Cette spirale est composée de demi-cercles de rayons croissants.

```python
from turtle import *

pensize(3)
speed(10)

n = 10
rayon = 0
for k in range(n):
    rayon += 10         # le rayon augmente à chaque tour
    circle(rayon, 180)  # un demi-cercle
    stamp()             # pour laisser une marque de la tortue

done()
```


Construire avec [Basthon](https://console.basthon.fr) une image de spirale composée de quarts de cercles de rayons décroissants, comme celle-ci :

![](images/spirale_2.svg){ .autolight }


## drawSvg

drawSvg permet de créer des images vectorielles avec Python en suivant le format des images SVG.

!!! info "Installation"
    Pour l'utiliser, l'administrateur l'installe avec :

    ```console
    $ pip install drawSvg
    ```

    :warning: Noter le `S` capital.

!!! tip "Utilisation sans installer"
    
    - Pour l'utiliser sans installation, on peut aller sur [JupyterLite](https://jupyterlite.readthedocs.io/en/latest/_static/lab/index.html), un carnet Jupyter en ligne.

    On ajoutera alors une première cellule :

    ```python
    import piplite
    await piplite.install('drawSvg')
    ```

    On pourra ainsi réaliser les figures demandées dans des cellules différentes.


!!! note "Principe"
    Pour les premières constructions, on définit une zone (`Drawing`) de la figure ; un rectangle dont on donne la largeur et la hauteur, des flottants.

    - Les points sont repérés avec des coordonnées cartésiennes `(x, y)`. Il n'y a pas de notion de pixel, les coordonnées peuvent parfaitement être des flottants.
    - Par défaut, l'origine est en bas à gauche, mais on peut la centrer, ou la décaler.

    === "Origine en bas à gauche"

        ```python
        import drawSvg as draw

        LARGEUR, HAUTEUR = 700.0, 500.0
        figure = draw.Drawing(LARGEUR, HAUTEUR)

        x, y, r = 0.0, 0.0, 200.0
        figure.append(draw.Circle(x, y, r, fill='black'))
        figure.saveSvg('fig_1.svg')
        ```
    
    === "Origine au centre"

        ```python
        import drawSvg as draw

        LARGEUR, HAUTEUR = 700.0, 500.0
        figure = draw.Drawing(LARGEUR, HAUTEUR, origin='center')
        ```

    === "Origine décalée"

        ```python
        import drawSvg as draw

        LARGEUR, HAUTEUR = 700.0, 500.0
        figure = draw.Drawing(LARGEUR, HAUTEUR, origin=(100.0, 100.0))
        ```

    


