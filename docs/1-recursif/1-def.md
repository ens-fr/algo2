---
author: Franck CHAMBON
---

# 💫 La récursivité

![](images/self_description.png)
> XKCD 688[^xkcd688] : Self-Description

[^xkcd688]: XKCD 688 : [Self-Description](https://xkcd.com/688/)

Récursivité
: Un concept est dit **récursif** lorsqu'il fait appel à lui-même dans sa propre définition.

## Exemples

### Dans la nature

Remarquons qu'un chou romanesco[^chouromanesco] est constitué d'un ensemble de « florettes » pyramidales disposées en couronnes spiralées. Sa forme géométrique (dite fractale) est très particulière et décorative. 

[^chouromanesco]: :material-wikipedia: [Chou romanesco](https://fr.wikipedia.org/wiki/Chou_romanesco)

![](images/600px-Romanesco_broccoli_(Brassica_oleracea).jpeg)

Par [Ivar Leidus](https://commons.wikimedia.org/w/index.php?curid=100133434) — Travail personnel, CC BY-SA 4.0



### La mise en abyme

!!! example inline end "Mise en abyme simple"
    ![](images/Droste_cacao_100gr_blikje%2C_foto_02.jpeg)
    > La boite de conserve originale du cacao de la marque Droste[^droste], en 1904.

    [^droste]: Cacao de la marque Droste : [Crédit image](https://commons.wikimedia.org/wiki/File:Droste_cacao_100gr_blikje,_foto_02.JPG)


La mise en abyme[^abyme] est un procédé consistant à représenter une œuvre dans une œuvre similaire, par exemple dans les phénomènes de « film dans un film », ou encore en incrustant dans une image cette image elle-même (en réduction). Ce principe se retrouve dans le phénomène ou le concept d'« autosimilarité », comme dans le principe des figures géométriques fractales ou du principe mathématique de la récursivité.

[^abyme]: :material-wikipedia: [Mise en abyme](https://fr.wikipedia.org/wiki/Mise_en_abyme)


On peut citer des exemples variés :

- **Littérature** (Il ne faut pas confondre récit enchâssé et mise en abyme). Don Quichotte[^donquichotte] est un jalon important de l'histoire littéraire et les interprétations qu'on en donne sont multiples : pur comique, satire sociale, analyse politique. Il est considéré comme l'un des romans les plus importants de la littérature mondiale et comme le premier roman moderne.
- **Théâtre**. La tragédie _Hamlet_, de Shakespeare : à l'intérieur de la pièce se joue une pièce de théâtre qui dénoncera l'adultère et le meurtre du père d'Hamlet[^hamlet].
- **Dessin**. On consultera des œuvres du graphiste Maurits Cornelis Escher, comme _Exposition d'estampes_[^escher] [^escher2].
- **Bande dessinée**. La série _Julius Corentin Acquefacques_ de Marc-Antoine Mathieu contient de nombreux exemples de mise en abyme.
- **Cinéma**. _Inception_ de Christopher Nolan, ou _Réalité_ de Quentin Dupieux.
- **Musique**. Le clip[^bachelorette] de la chanson _Bachelorette_ de Björk se base sur un principe de mise en abyme à plusieurs niveaux.
- **Divers**. Les poupées russes. La publicité pour _La vache qui rit_. La pochette de _Ummagumma_, le quatrième album de Pink Floyd.

[^donquichotte]: :material-wikipedia: [Don Quichotte](https://fr.wikipedia.org/wiki/Don_Quichotte)
[^escher]: Pour la Science, [La galerie en abyme d'Escher](https://www.pourlascience.fr/sd/art-science/la-galerie-en-abyme-d-escher-5103.php)
[^escher2]: [Site de la fondation Escher](https://mcescher.com/gallery/most-popular/#iLightbox[gallery_image_1]/24)
[^bachelorette]: :material-youtube: [björk : bachelorette](https://www.youtube.com/watch?v=JNJv-Ebi67I), réalisé par Michel Gondry
[^hamlet]: [Wiki Littérature, Hamlet](https://litterature.fandom.com/fr/wiki/Hamlet)


!!! example "Mise en abyme multiple"
    ![](images/vache.gif)
    > Création de l'artiste visuel Polonais Feliks Tomasz Konczakowski[^Konczakowski]

    [^Konczakowski]: [Galerie Tumblr](https://konczakowski.tumblr.com/) de Feliks Tomasz Konczakowski


### Sigles récursifs

!!! info inline end "Les sigles"
    Un sigle[^sigle] est un ensemble de lettres initiales majuscules qui, épelé ou prononcé de manière syllabique, forme un mot servant d'abréviation.

    [^sigle]: :material-wikipedia: [Sigle](https://fr.wikipedia.org/wiki/Sigle)

    Souvent, un sigle sert de définition, comme :

    - SNCF (La **S**ociété **n**ationale des **c**hemins de fer **f**rançais), ou 
    - FFV (La **F**édération **f**rançaise de **v**oile)

    On trouve des sigles qui s'utilisent eux-même dans leur définition...

!!! warning "Attention"
    Ces sigles, par jeu, ne peuvent pas donner une définition précise.

Visa, un système de paiement
: _**V**isa **I**nternational **S**ervice **A**ssociation_.

PHP, un langage de serveur
: _**P**HP: **H**ypertext **P**reprocessor_.

GNU, un projet de logiciels libres
: _**G**NU is **N**ot **U**nix_.

!!! cite "Une tradition culturelle"
    Nous, les hackers, nous [...] avions aussi une tradition d'acronymes récursifs qui consiste à dire que le programme qu'on crée est similaire à un programme existant.

    ---

    Richard Stallman, créateur du projet GNU


### En informatique

En informatique, on trouve :

- des fonctions récursives, l'objet de ce chapitre ;
- des structures de données récursives, comme une pile ou certains arbres, l'objet de chapitres suivants ;
- des fractales ; nous apprendrons à en dessiner certaines.

!!! example inline "le flocon de Koch"
    Le flocon de Koch[^koch] est l'une des premières courbes fractales à avoir été décrites, bien avant l'invention du terme « fractal(e) » par Benoît Mandelbrot.

    [^koch]: :material-wikipedia: [Le flocon de Koch](https://fr.wikipedia.org/wiki/Flocon_de_Koch)

    ![](images/Koch_anime.gif)

    Elle a été inventée en 1904 par le mathématicien suédois Helge von Koch.


!!! example inline end "La fougère de Barnsley"

    La fougère de Barnsley[^barnsley] est une fractale nommée d'après le mathématicien Michael Barnsley qui l'a décrite pour la première fois dans son livre Fractals Everywhere.

    [^barnsley]: :material-wikipedia: [La fougère de Barnsley](https://fr.wikipedia.org/wiki/Foug%C3%A8re_de_Barnsley)

    ![](images/Barnsleys_fern.png)

!!! example "L'arbre de Pythagore"

    L'arbre de Pythagore[^arbrePythagore] est une fractale plane construite à l'aide de carrés. Elle porte le nom de Pythagore car chaque triplet de carrés en contact enclot un triangle rectangle, une configuration traditionnellement utilisée pour illustrer le théorème de Pythagore. 

    ![](images/Pythagoras_tree_1_1_13_Summer.svg)

    [^arbrePythagore]: :material-wikipedia: [L'arbre de Pythagore](https://fr.wikipedia.org/wiki/Arbre_de_Pythagore)


![](images/fixing_problems.png)

> XKCD 1739[^xkcd1739] : Fixing Problems

[^xkcd1739]: XKCD 1739 : [Fixing Problems](https://xkcd.com/1739/)


!!! cite "_Easter egg_"

    Avec un moteur de recherche, en cas de faute d'orthographe, on peut avoir des suggestions... ici il n'y a pas de faute :wink:

    ![](images/recursif-dark.png#only-dark)
    ![](images/recursif-light.png#only-light)

!!! info "Humour"
    Pour bien comprendre la récursivité, vous devez d'abord bien comprendre ce qu'est la [récursivité](#la-recursivite).

## Fonctions récursives

### Définition

Une fonction est dite récursive si elle contient un appel à elle-même dans sa propre définition.

!!! faq "Quelles conséquences ?"
    Devinez ce que font les scripts suivants, ils contiennent des fonctions récursives :

    === "f_1"

        ```python
        def f_1():
            print("Bonjour")
            f_1()
        
        f_1()
        ```

        ??? done "Réponse"
            Ce script affiche `Bonjour` de nombreuses fois.

            :warning: En théorie jusqu'à l'infini, mais Python arrête l'exécution au bout d'un moment et affiche un message d'erreur.

    === "f_2"

        ```python
        def f_2():
            f_2()
            print("Bonjour")
        
        f_2()
        ```

        ??? done "Réponse"
            Ce script n'affiche jamais `Bonjour`, mais...
            
            :warning: Il y a de nombreux appels récursifs, jusqu'au message d'erreur de Python.


    === "f_3"

        ```python
        def f_3():
            if False:
                f_3()
            print("Bonjour")
        
        f_3()
        ```

        ??? done "Réponse"
            Ce script affiche `Bonjour` une seule fois. Cette fonction est récursive (par définition), mais il n'y a jamais d'appels récursifs (par construction).


    === "f_4"

        ```python
        def f_4(n):
            print(n)
            if n > 0:
                f_4(n - 1)
        
        f_4(4)
        ```

        ??? done "Réponse"
            Ce script affiche

            ```
            4
            3
            2
            1
            0
            ```


![](images/ozymandias.png)
> XKCD 1557[^xkcd1557]

[^xkcd1557]: XKCD 1557 : [Ozymandias](https://xkcd.com/1557/)



### Petits exercices


#### Écrire une punition

On veut créer une fonction `punition` qui écrit `n` fois un certain `texte`.

- `n` est un entier
- `texte` est une chaine de caractères

```pycon
>>> punition(5, "Je dois suivre en classe.")
Je dois suivre en classe.
Je dois suivre en classe.
Je dois suivre en classe.
Je dois suivre en classe.
Je dois suivre en classe.
```

!!! example "Voici plusieurs versions"

    === "itérative"

        ```python
        def punition(n, texte):
            for i in range(n):
                print(texte)
        ```

        Cette méthode est ici simple, avec une boucle qui répète `n` fois une instruction.

    === "récursive 1"

        ```python
        def punition(n, texte):
            print(texte)
            punition(n - 1, texte)
        ```

        :warning: Version **fausse**

        L'appel à `punition(3, texte)` donne ensuite les appels :

        - `punition(2, texte)`
        - `punition(1, texte)`
        - `punition(0, texte)`
        - `punition(-1, texte)`
        - `punition(-2, texte)`
        - `punition(-3, texte)`
        - ... sans limite, ou presque.

        Python donnerait alors un message d'erreur que nous verrons plus tard.

    === "récursive 2"

        ```python
        def punition(n, texte):
            if n > 0:
                print(texte)
                punition(n - 1, texte)
        ```

        Le principe de cette version récursive correcte est :

        - si `n` est strictement positif,
            - on écrit le `texte`,
            - puis il reste à faire la punition `n - 1` fois.
        - sinon, il n'y a rien à faire ;

    === "récursive 3"

        ```python
        def punition(n, texte):
            if n > 0:
                punition(n - 1, texte)
                print(texte)
        ```

        Le principe de cette autre version récursive correcte est :

        - si `n` est strictement positif,
            - on fait la punition `n - 1` fois,
            - il reste alors une fois le `texte` à écrire.
        - sinon, il n'y a rien à faire ;

!!! info "Est-ce plus complexe ?"

    - parfois la récursivité est inutile ;
    - parfois la récursivité est nettement plus simple ;
    - parfois la récursivité est presque indispensable, avec des structures de données... récursives.

    :warning: Il faudra faire attention à ce que la fonction récursive s'arrête !

#### Vérifier un mot de passe

On veut une fonction `verifie_mdp` pour vérifier le mot de passe d'un identifiant.

On suppose qu'on dispose déjà de fonctions `demande_mdp` et `message_erreur`.

Les deux versions suivantes sont-elles équivalentes ou non ?

```python
def verifie_mdp():
    # Version itérative
    mdp = demande_mdp()
    while mdp != "secret_1234":
        message_erreur()
        mdp = demande_mdp()
```

```python
def verifie_mdp():
    # Version récursive
    mdp = demande_mdp()
    if mdp != "secret_1234":
        message_erreur()
        verifie_mdp()
```

??? done "Réponse"
    Oui, presque équivalente.
    
    La différence principale étant qu'au bout de 1000 erreurs, Python arrêtera la fonction récursive, alors que l'itérative peut continuer à l'infini.

#### Somme des premiers entiers

On veut créer une fonction `somme` qui **renvoie** la somme des entiers de `1` à `n` inclus.

- `n` est un entier
- Exemples :
    - $1+2+3+4 = 10$, donc `somme(4)` renvoie `10`.
    - $1+2+3+4+5 = 15$, donc `somme(5)` renvoie `15`.

On pourra constater que 

- `somme(5)` est égal à `5 + somme(4)`
- de manière générale, pour `n > 0`, `somme(n)` est égal à `n + somme(n - 1)`


```pycon
>>> somme(0)
0
>>> somme(1)
1
>>> somme(2)
3
>>> somme(3)
6
>>> somme(4)
10
```

Voici plusieurs versions, à vous de dire, pour chacune, si elle est itérative ou récursive, correcte ou fausse.

=== "version 1"

    ```python
    def somme(n):
        for i in range(n):
            total += i
        return total
    ```

    ??? done "Réponse"
        Il y a deux erreurs dans cette **version itérative** :

        1. il faut initialiser `total` à `0` avant la boucle ;
        2. l'entier `n` n'est pas ajouté, pour corriger :
            - soit on fait un tour de boucle en plus,
            - soit on ajoute `i + 1` à chaque tour au lieu de `i`,
            - soit, **mieux**, on fait une boucle de `1` inclus à `n + 1` exclu.

=== "version 2"

    ```python
    def somme(n):
        print(n + somme(n - 1))
    ```

    ??? done "Réponse"
        Il y a deux erreurs dans cette **version récursive** :

        1. il faut une structure conditionnelle pour renvoyer `0` si `n` est négatif ;
        2. il faut **renvoyer** le résultat et non l'afficher.

=== "version 3"

    ```python
    def somme(n):
        return n * (n + 1) / 2
    ```

    ??? done "Réponse"
        Il y a deux erreurs dans cette **version avec une formule** :

        1. il faut une structure conditionnelle pour renvoyer `0` si `n` est négatif ;
        2. le résultat sera ici un flottant, si `n` est gigantesque le résultat sera arrondi ; il faut utiliser une division entière avec `// 2`

        Remarque : cette formule est au programme de la spécialité mathématiques, en première.

À vous de compléter la fonction ci-dessous pour qu'elle passe les tests.

{{ IDE('somme') }}

??? done "Solution itérative"

    ```python
    def somme(n):
        total = 0
        for i in range(1, n + 1):
            total += i
        return total

    assert somme(0) == 0
    assert somme(1) == 1
    assert somme(3) == 6
    assert somme(5) == 15
    ```

??? done "Solution récursive"

    ```python
    def somme(n):
        if n <= 0:
            return 0
        else:
            return somme(n - 1) + n


    assert somme(0) == 0
    assert somme(1) == 1
    assert somme(3) == 6
    assert somme(5) == 15
    ```


#### Affichage d'intervalle

On veut créer une fonction `affiche_intervalle` qui affiche les entiers de `mini` à `maxi` tous deux inclus.

- `mini` et `maxi` sont des entiers
- si `maxi < mini`, il n'y a rien à afficher

Voici trois solutions récursives d'élèves, sont-elles justes ou fausses, améliorables ?


=== "Version 1"

    ```python
    def affiche_intervalle(mini, maxi):
        if mini == maxi + 1:
            return maxi
        else:
            print(mini)
            return affiche_intervalle(mini + 1, maxi)
    ```

    ??? done "Réponse"
        1. La fonction devrait renvoyer `#!py None` implicitement, et non une valeur ; il ne faut pas de `#!py return` dans cette fonction.
        2. La condition doit être revue ; les appels récursifs ne s'arrêtent pas si `mini > maxi + 1`.
    

=== "Version 2"

    ```python
    def affiche_intervalle(mini, maxi):
        if mini == maxi:
            print(mini)
        elif mini > maxi:
            return None
        else:
            affiche_intervalle(mini, maxi - 1)
            print(maxi)
    ```

    ??? done "Réponse"
        1. C'est correct.
        2. Mais on peut simplifier,
            - le premier cas est inutile,
            - le second peut se simplifier encore,
            - le troisième peut s'écrire de deux façons.



=== "Version 3"

    ```python
    def affiche_intervalle(mini, maxi):
        "Affiche, un par ligne, les entiers de mini à maxi inclus"

        if mini <= maxi:
            print(mini)
            affiche_intervalle(mini + 1, maxi)
    ```

    ??? done "Réponse"
        C'est parfait.

        Il y a même une docstring, c'est une bonne pratique à adopter pour toute fonction.

#### Retournement d'une chaine

Écrire une fonction récursive qui renvoie le retournement d'une chaine de caractères.

!!! tip "Indices"
    Si une chaine `texte` est non vide :

    - `texte[0]` correspond au premier caractère.
    - `texte[1:]` est une copie de la suite.

{{ IDE('retourne') }}

??? done "Réponse"

    ```python
    def retourne(truc):
        "Renvoie une copie en miroir de `texte`"
        if len(texte) == 0:
            return texte
        else:
            return retourne(texte[1:]) + texte[0]

    assert retourne('salut') == 'tulas'
    ```
