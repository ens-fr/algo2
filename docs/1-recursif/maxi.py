def maxi(nombres):
    ...


assert maxi([7]) == 7
assert maxi([7, 13]) == 13
assert maxi([13, 7]) == 13
assert maxi([-19, -13, -19]) == -13
