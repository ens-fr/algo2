def sierpinski(n):
    ...
    
    
assert sierpinski(1) == [
    "#",
]

assert sierpinski(2) == [
    "##",
    "#",
]

assert sierpinski(8) == [
    "########",
    "# # # #",
    "##  ##",
    "#   #",
    "####",
    "# #",
    "##",
    "#",
]

