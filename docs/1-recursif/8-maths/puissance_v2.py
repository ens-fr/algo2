def puissance_v2(a: float, n: int) -> float:
    "Renvoie `a`  à la puissance `n`, pour n >= 0"
    # méthode récursive
    ...

assert puissance_v2(3.0, 0) == 1.0
assert puissance_v2(3.0, 1) == 3.0
assert puissance_v2(3.0, 4) == 81.0
assert puissance_v2(3.0, 5) == 243.0
assert puissance_v2(2.0, 10) == 1024.0

