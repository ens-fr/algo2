# La fonction factorielle

Pour un entier naturel $n$, on note $n! = 1 × 2 × 3 × 4 × ... × (n - 1) × n$, le produit des entiers naturels jusqu'à $n$, sauf zéro.

Ainsi

- $4! = 1 × 2 × 3 × 4 = 24$
- $5! = 1 × 2 × 3 × 4 × 5 = 120$

On a aussi 

- $1! = 1$, il y a un seul facteur égal à $1$,
- $0! = 1$, comme un produit vide.

{{ IDE('factorielle') }}

!!! note "Question 1"
    Donnez une version itérative d'une fonction `factorielle` qui prend un argument `n` entier naturel ; on garantit que `n >= 0`.

??? tip "Indice 1"
    On pourra faire une boucle pour `x` allant de `1` inclus à `n + 1` exclu.

??? done "Réponse"

    ```python
    def factorielle(n: int) -> int:
        "Renvoie la factorielle de n >= 0"
        y = 1
        for x in range(1, n + 1):
            y = y * x
        return y
    ```

!!! note "Question 2"
    Donnez une version récursive de `factorielle`.

??? tip "Indice 2"
    On pourra constater que $5! = 4! × 5$, et que, de manière générale,

    pour $n > 0$, on a $n! = (n - 1)! × n$

??? done "Réponse 2"

    ```python
    def factorielle(n: int) -> int:
        "Renvoie la factorielle de n >= 0"
        if n == 0:
            return 1
        else:
            return factorielle(n - 1) * n
    ```


    