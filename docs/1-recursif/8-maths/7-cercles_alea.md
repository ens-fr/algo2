
### Cercles aléatoires

> Pour les élèves en spé maths. :warning: À déplacer dans la section maths !!!

**Challenge**, reproduire une figure semblable à :

![](./fractales/cercle_alea_1.svg){ .autolight }

??? done "Réponse"

    ```python
    import drawSvg as draw
    from random import random
    from math import pi, cos, sin

    d = draw.Drawing(500, 500)

    def motif(x, y, r, e, n):
        """Dessine un motif,
            - avec un premier cercle (x, y, r) épaisseur e
            - un motif intérieur réduit de 1/2
            - un nb aléatoire de motifs entre le cercle et le motif central
                - répartition aléatoire sur 360°
        """
        d.append(draw.Circle(x, y, r,
                fill='none', stroke_width=e, stroke='black'))

        if n > 0:
            motif(x, y, r/2, e/2, n-1)
            
            while random() > 0.2:
                alpha = 2 * pi * random()
                x_ = x + 0.75 * cos(alpha) * r
                y_ = y + 0.75 * sin(alpha) * r
                motif(x_, y_, r/4, e/4, n-1)

    r = 400
    x, y = 250, 250     # un peu de marge
    n = 6               # profondeur de récursion
    e = 15              # épaisseur du premier trait
    motif(x, y, r, e, n)

    d
    ```

