def puissance_v1(a: float, n: int) -> float:
    "Renvoie `a` à la puissance `n`, pour n >= 0"
    ...


assert puissance_v1(3.0, 0) == 1.0
assert puissance_v1(3.0, 1) == 3.0
assert puissance_v1(3.0, 4) == 81.0
assert puissance_v1(3.0, 5) == 243.0
assert puissance_v1(2.0, 10) == 1024.0

