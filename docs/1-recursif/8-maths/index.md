
## Pour les NSI + spé Maths

1. Fonction factorielle
2. a puissance n, en O(n)
2. a puissance n, en O(log(n))
2. Somme des chiffres d'un entier
2. Nombre de bits égaux à 1
3. Fonction d'Ackermann
5. Somme de somme de somme... des chiffres d'un entier

## Pour les NSI + maths experts

1. PGCD de deux entiers
2. GEB, plusieurs suites...
3. France-IOI ; Niveau 4, récursivité avancée (permutations, coef binomial...)
4. Nombre de façons d'écrire un entier comme une somme (ordre compte ; facile)
4. Nombre de façons d'écrire un entier comme une somme (ordre ne compte pas ; difficile)


## Pour les artistes 3D

1. Cube de Sierpinski, **guidé**, en 3D avec Asymptote



## Pour les post BAC

- Calcul de complexité
- Évocation de la récursion terminale
