def factorielle(n: int) -> int:
    "Renvoie la factorielle de n >= 0"
    ...


assert factorielle(0) == 1
assert factorielle(1) == 1
assert factorielle(4) == 24
assert factorielle(5) == 120


