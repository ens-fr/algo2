# La fonction puissance, v2

On souhaite améliorer la vitesse de calcul de $a^n$, pour $a$ flottant et $n$ entier naturel.

On constate que 

- $a^{2022} = {a^1011} × {a^1011}
- $a^{2023} = {a^2022} × a

et que de manière générale

- si $n$ est pair, $a^n = a^{n/2} × a^ {n/2}$, avec $n/2$ qui est bien entier
- sinon, $a^n = a^{n-1} × a$

{{ IDE('puissance_v2') }}

!!! note "Question"
    Donnez une version récursive de `puissance_v2` utilisant ce principe.

??? tip "Indice"
    On n'oubliera pas le cas de base `n = 0`.

??? done "Réponse"

    ```python
    def puissance_v2(a: float, n: int) -> float:
        "Renvoie `a`  à la puissance `n`, pour n >= 0"
        if n == 0:
            # cas de base
            return 1.0
        elif n % 2 == 0:
            # n est pair
            y = puissance_v2(a, n // 2)
            return y * y
        else:
            # n est impair
            return puissance_v2(a, n - 1) * a
    ```

!!! info "Compléxité"
    Le coût du calcul de `puissance_v2(a, n)` est inférieur à deux fois le nombre de bits de `n`.

    TODO : dessiner un arbre d'appels récursifs...

    On note alors ce coût $\mathcal O(\log(n))$, ainsi pour tout flottant et tout entier sur 32 bits, il y aura moins de 64 multiplications, ce qu irend cette version **bien** plus performante que `puissance_v1(a, n)`.
    