from svg_turtle import SvgTurtle
# pour des sorties Tortues SVG

t = SvgTurtle(215, 215)

k = 2/3

def branche(x, n):
    angle = 30
    x_1 = x * (1-k)
    x_2 = x * k
    t.pensize(x / 20)
    t.forward(x)
    t.penup()
    t.backward(x_2)
    t.pendown()
    if n > 0:
        t.left(angle)
        branche(x_2, n - 1)
        t.right(2 * angle)
        branche(x_2, n - 1)
        t.left(angle)
    t.backward(x_1)


for _ in range(6):
    branche(100, 5)
    t.right(60)

t.save_as('flocon_neige.svg')
