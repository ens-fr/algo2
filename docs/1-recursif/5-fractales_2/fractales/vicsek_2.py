import drawSvg as draw

d = draw.Drawing(500, 500, origin='center')

def vicsek_2(x, y, a, n):
    if n > 0:
        # Un carré
        case = draw.Rectangle(x - a/2, y - a/2, a, a)

        # pour en faire une animation ;-)
        case.appendAnim(draw.AnimateTransform('scale', '10s',
        '1, 1; 1.5, 1.5; 3, 3; 9, 9; 27, 27; 81, 81; 27, 27; 9, 9; 3, 3; 1.5, 1.5; 1, 1',
                            repeatCount='indefinite'))

        d.append(case)

        a /= 3
        for dx, dy in [(-a, -a), (a, -a), (-a, a), (0, 0), (a, a)]:
            vicsek_2(x + dx, y + dy, a, n - 1)


a = 400
x, y = 0, 0     # un peu de marge
n = 5           # profondeur de récursion

vicsek_2(x, y, a, n)

d.saveSvg('vicsek_2.svg')
