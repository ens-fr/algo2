import drawSvg as draw
from math import sqrt

LARGEUR, HAUTEUR = 700.0, 500.0

figure = draw.Drawing(LARGEUR, HAUTEUR, origin='center')

def cercle(x: float, y: float, r: float):
    e = r * 0.02  # épaisseur du trait
    figure.append(draw.Circle(
            x, y, r,
        stroke_width=e,
        stroke='black',
        fill='none',
    ))

k = (4 - sqrt(7)) / 3

def gardi(a: float, est_vertical: bool, n: int):
    if n > 0:
        if est_vertical:
            x, y = a, 0
        else:
            x, y = 0, a
        r = 2 * a
        cercle(+x, +y, r)
        cercle(-x, -y, r)

        gardi(a * k, not(est_vertical), n - 1)

a = 100.0
n = 6
gardi(a, True, n)

figure.saveSvg('gardi.svg')
figure  # pour affichage dans un carnet
