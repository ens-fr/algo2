import drawSvg as draw

d = draw.Drawing(500, 500)

def motif(x, y, r, e, n):
    """Dessine un motif,
        - avec un premier cercle (x, y, r)
        - un nez en +
        - une bouche en =
        - tout cela d'épaisseur e
        - deux yeux récursifs semblable au modèle,
        - facteur récursif ×0.35
    """
    # tête
    d.append(draw.Circle(x, y, 5*r,
            fill='none', stroke_width=e, stroke='black'))

    if n > 0:
        # nez
        dh = r
        d.append(draw.Lines(x + dh, y,      x - dh, y,
            stroke_width=e, stroke='black'))
        d.append(draw.Lines(x,      y + dh, x,      y - dh,
            stroke_width=e, stroke='black'))

        # bouche
        dx = r
        dy = -3 * r
        dh = r
        d.append(draw.Lines(x - dx, y + dy + dh, x + dx , y + dy + dh,
            stroke_width=e, stroke='black'))
        d.append(draw.Lines(x - dx, y + dy     , x + dx , y + dy     ,
            stroke_width=e, stroke='black'))


        # yeux
        dx = 2.5 * r
        dy = 1.5 * r
        y_ = y + dy
        r_ = 0.35 * r
        e_ = 0.35 * e
        motif(x - dx, y_, r_, e_, n - 1)
        motif(x + dx, y_, r_, e_, n - 1)


r = 44
x, y = 250, 250     # un peu de marge
n = 4               # profondeur de récursion
e = 11              # épaisseur du premier trait
motif(x, y, r, e, n)

d.saveSvg('toto.svg')
