import drawSvg as draw

d = draw.Drawing(500, 500, origin='center')

def vicsek_1(x, y, a, n):
    if n > 0:
        # un carré
        d.append(draw.Rectangle(x - a/2, y - a/2, a, a))
        a /= 3
        for dx, dy in [(-a, 0), (0, -a), (a, 0), (0, 0), (0, a)]:
            vicsek_1(x + dx, y + dy, a, n - 1)


a = 400
x, y = 0, 0
n = 6           # profondeur de récursion

vicsek_1(x, y, a, n)

d.saveSvg('vicsek_1.svg')
