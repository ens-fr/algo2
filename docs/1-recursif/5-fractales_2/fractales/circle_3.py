import drawSvg as draw

d = draw.Drawing(500, 350)

def motif(x, y, r, dx, dy, k, e, n):
    """Dessine un motif, avec un premier cercle (x, y, r)
    puis d'autres dans les 4 directions
        sauf sens d'origine indiqué par (dx, dy)
    L'épaisseur du trait est réduit avec ×0.65 au niveau suivant
    Le rayon des cercles est réduit avec ×k au niveau suivant
    """

    if n > 0:
        d.append(draw.Circle(x, y, r,
                fill='none', stroke_width=e, stroke='black'))

        decalage = r + k*r
        r_ = r * k
        e_ = e * 0.65
        for dx_, dy_ in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            if (dx_, dy_) != (dx, dy):
                x_ = x + decalage * dx_
                y_ = y + decalage * dy_
                motif(x_, y_, r_, -dx_, -dy_, k, e_, n - 1)


r = 100
x, y = 250, r + 10  # un peu de marge
k = 0.4             # facteur de réduction des cercles
n = 8               # profondeur de récursion
dx, dy = 0, -1      # au début, on vient du Sud
e = 3               # épaisseur du premier trait
motif(x, y, r, dx, dy, k, e, n)

d.saveSvg('circle_3.svg')
