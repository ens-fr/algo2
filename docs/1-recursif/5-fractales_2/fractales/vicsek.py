import drawSvg as draw

d = draw.Drawing(700, 200)

def vicsek_1(x, y, a, n):
    if n == 0:
        # un carré
        d.append(draw.Rectangle(x - a/2, y - a/2, a, a))
    else:
        a /= 3
        for dx, dy in [(-a, 0), (0, -a), (a, 0), (0, 0), (0, a)]:
            vicsek_1(x + dx, y + dy, a, n - 1)


def vicsek_2(x, y, a, n):
    if n == 0:
        # Un carré
        d.append(draw.Rectangle(x - a/2, y - a/2, a, a))
    else:
        a /= 3
        for dx, dy in [(-a, -a), (a, -a), (-a, a), (0, 0), (a, a)]:
            vicsek_2(x + dx, y + dy, a, n - 1)


for n in range(6):
    a = 80
    x, y = 100, 50     # un peu de marge

    vicsek_1(x + n*100, y + 100, a, n)
    vicsek_2(x + n*100, y      , a, n)

d.saveSvg('vicsek.svg')
