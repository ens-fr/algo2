from svg_turtle import SvgTurtle
# pour des sorties Tortues SVG

t = SvgTurtle(1000, 1400)


def invert(pliage):
    return "".join(["G" if x == "D" else "D" for x in pliage[::-1]])


def dragon(n):
    pliage = ""
    for _ in range(n):
        pliage = pliage + "G" + invert(pliage)
    for x in pliage:
        t.forward(4)
        t.left(90 if x == "G" else -90)


dragon(14)

t.save_as('dragon_2.svg')
