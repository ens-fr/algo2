from svg_turtle import SvgTurtle
# pour des sorties Tortues SVG

t = SvgTurtle(800, 300)

from math import sqrt
k = sqrt(0.5)

def dragon_gauche(d, n):
    if n == 0:
        t.forward(d)
    else:
        d *= k
        t.left(45)
        dragon_droite(d, n - 1)
        t.right(90)
        dragon_gauche(d, n - 1)
        t.left(45)

def dragon_droite(d, n):
    if n == 0:
        t.forward(d)
    else:
        d *= k
        t.right(45)
        dragon_droite(d, n - 1)
        t.left(90)
        dragon_gauche(d, n - 1)
        t.right(45)
    

def motif(d, n):
    d_ = d * k
    t.penup()

    t.left(90)
    t.forward(d/2)
    t.right(90)
    t.pendown()
    Z = t.pos()
    
    # GAUCHE
    t.color('yellow')
    t.pensize(1)
    dragon_gauche(d, n - 1)
    t.penup()
    t.goto(Z)

    t.pensize(3)
    t.pendown()
    t.left(45)
    t.color('blue')
    dragon_droite(d_, n - 1)
    t.right(90)
    t.color('red')
    dragon_gauche(d_, n - 1)
    t.left(45)
    t.penup()
    t.goto(Z)

    t.right(90)
    t.forward(d)
    t.left(90)
    t.pendown()
    Z = t.pos()
    
    # DROITE
    t.color('yellow')
    t.pensize(1)
    dragon_droite(d, n - 1)
    t.penup()
    t.goto(Z)

    t.pensize(3)
    t.pendown()
    t.right(45)
    t.color('blue')
    dragon_droite(d_, n - 1)
    t.left(90)
    t.color('red')
    dragon_gauche(d_, n - 1)
    t.right(45)


    t.penup()
    t.goto(Z)
    



    t.left(90)
    t.forward(d/2)
    t.right(90)


t.penup()
t.bk(370)
t.pendown()
for n in range(1, 6):
    motif(100, n)
    t.forward(150)

t.save_as('dragon.svg')
