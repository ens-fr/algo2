from svg_turtle import SvgTurtle
# pour des sorties Tortues SVG

t = SvgTurtle(215, 215)

def branche(x, n):
    angle = 30
    x_ = x / 4
    t.pensize(x / 20)
    t.forward(x)

    t.penup()
    t.backward(2 * x_)
    t.pendown()
    if n > 0:
        t.left(angle)
        branche(2 * x_, n - 1)
        t.right(2 * angle)
        branche(2 * x_, n - 1)
        t.left(angle)
    
    t.backward(x_)
    if n > 1:
        t.left(angle)
        branche(x_, n - 2)
        t.right(2 * angle)
        branche(x_, n - 2)
        t.left(angle)
    
    t.backward(x_)


for _ in range(6):
    branche(100, 5)
    t.right(60)

t.save_as('flocon_neige_2.svg')
