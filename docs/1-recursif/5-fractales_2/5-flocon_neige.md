---
author: Franck CHAMBON
---

# 🐢 Flocon de neige

TODO ; à refaire mieux

**Challenge** : Construire cette figure symétrique, à six branches.

!!! tip "Indices"
    - L'épaisseur d'une sous-branche est 5% de la longueur de la tige centrale.
    - Une branche de niveau 0 est un segment.
    - Sinon, chaque branche possède au premier tiers :
        - une branche qui part à gauche, une autre à droite,
        - l'angle est de 30°.

![](./fractales/flocon_neige.svg){ .autolight }

