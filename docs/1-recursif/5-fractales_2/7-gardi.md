---
author: Franck CHAMBON
---

# 📐 Cercles tangents

**Challenge** : Tracer la figure suivante.

![](./fractales/gardi.svg){ .autolight }

!!! info "Méthode"
    On trace deux cercles de même rayon et passant par le centre de l'autre.

    Dans la zone d'intersection on place deux tels cercles, les plus grands possibles, puis d'autres de manière récursive...

    Il y a ici $2×6$ cercles.

??? tip  "Indice 1"
    Commencer par tracer les deux premiers cercles. On pourra compléter le code :

    ```python
    import drawSvg as draw
    from math import sqrt

    LARGEUR, HAUTEUR = 700.0, 500.0

    figure = draw.Drawing(LARGEUR, HAUTEUR, origin='center')

    def cercle(x: float, y: float, r: float):
        e = r * 0.02  # épaisseur du trait
        figure.append(draw.Circle(
                x, y, r,
            stroke_width=e,
            stroke='black',
            fill='none',
        ))

    def gardi(a: float):
        cercle(..., ..., ...)
        cercle(..., ..., ...)

    gardi(100.0)

    figure.saveSvg('gardi.svg')
    figure  # pour affichage dans un carnet
    ```

??? tip "Indice 2"
    Le coefficient de réduction des rayons successifs est $\dfrac{4-\sqrt7}3$.

    Cela se démontre en quelques lignes, niveau spé maths première :

    - Par symétrie, déterminer l'angle formé du centre aux points de tangence.
    - Prouver l'alignement de points entre un point de tangence, un centre et ...
    - Résoudre une équation de degré 2.

??? tip "Indice 3"
    Mettre en place une option `est_vertical` à la fonction `gardi`.

    Objectif : avoir les 4 premiers cercles.

    ```python
    import drawSvg as draw
    from math import sqrt

    LARGEUR, HAUTEUR = 700.0, 500.0

    figure = draw.Drawing(LARGEUR, HAUTEUR, origin='center')

    def cercle(x: float, y: float, r: float):
        e = r * 0.02  # épaisseur du trait
        figure.append(draw.Circle(
                x, y, r,
            stroke_width=e,
            stroke='black',
            fill='none',
        ))

    def gardi(a: float, est_vertical: bool):
        ...
        cercle(..., ..., ...)
        cercle(..., ..., ...)

    k = (4 - sqrt(7)) / 3
    a = 100.0

    gardi(a, True)
    gardi(a * k, False)

    figure.saveSvg('gardi.svg')
    figure  # pour affichage dans un carnet
    ```

??? tip "Indice 4"
    Mettre en place une version récursive pour obtenir une profondeur `n` avec $2n$ cercles au total.

    ```python
    import drawSvg as draw
    from math import sqrt

    LARGEUR, HAUTEUR = 700.0, 500.0

    figure = draw.Drawing(LARGEUR, HAUTEUR, origin='center')

    def cercle(x: float, y: float, r: float):
        e = r * 0.02  # épaisseur du trait
        figure.append(draw.Circle(
                x, y, r,
            stroke_width=e,
            stroke='black',
            fill='none',
        ))

    k = (4 - sqrt(7)) / 3

    def gardi(a: float, est_vertical: bool, n: int):
        if n > 0:
            if est_vertical:
                x, y = a, 0
            else:
                x, y = 0, a
            r = 2 * a
            cercle(+x, +y, r)
            cercle(-x, -y, r)
            ...
            gardi(..., ..., n - 1)

    a = 100.0
    n = 5
    gardi(a, True, n)

    figure.saveSvg('gardi.svg')
    figure  # pour affichage dans un carnet
    ```

??? done "Réponse"

    ```python
    import drawSvg as draw
    from math import sqrt

    LARGEUR, HAUTEUR = 700.0, 500.0

    figure = draw.Drawing(LARGEUR, HAUTEUR, origin='center')

    def cercle(x: float, y: float, r: float):
        e = r * 0.02  # épaisseur du trait
        figure.append(draw.Circle(
                x, y, r,
            stroke_width=e,
            stroke='black',
            fill='none',
        ))

    k = (4 - sqrt(7)) / 3

    def gardi(a: float, est_vertical: bool, n: int):
        if n > 0:
            if est_vertical:
                x, y = a, 0
            else:
                x, y = 0, a
            r = 2 * a
            cercle(+x, +y, r)
            cercle(-x, -y, r)

            gardi(a * k, not(est_vertical), n - 1)

    a = 100.0
    n = 6
    gardi(a, True, n)

    figure.saveSvg('gardi.svg')
    figure  # pour affichage dans un carnet
    ```
