---
author: Franck CHAMBON
---

# 📐 Fractale de Vicsek

On part d'un carré, on le découpe en 9, et on ne conserve que 5 des neuf morceaux. Et on recommence...

En observant ces étapes :

![](./fractales/vicsek.svg){ .autolight }

Construire cette image :

![](./fractales/vicsek_1.svg){ .autolight }

!!! tip "Indices"

    Pour dessiner un carré de centre `(x, y)`, parallèle aux axes et de côté `a` :
    
    ```python
    draw.Rectangle(x - a/2, y - a/2, a, a)
    ```

??? done "Réponse"

    ```python
    import drawSvg as draw

    figure = draw.Drawing(500, 500, origin='center')

    def vicsek_1(x, y, a, n):
        if n > 0:
            # un carré
            figure.append(draw.Rectangle(x - a/2, y - a/2, a, a))
            a /= 3
            for dx, dy in [(-a, 0), (0, -a), (a, 0), (0, 0), (0, a)]:
                vicsek_1(x + dx, y + dy, a, n - 1)


    a = 400
    x, y = 0, 0
    n = 6           # profondeur de récursion

    vicsek_1(x, y, a, n)

    figure
    ```

Ou sa variante, ici animée :

![](./fractales/vicsek_2.svg){ .autolight }

??? done "Réponse"

    ```python
    import drawSvg as draw

    figure = draw.Drawing(500, 500, origin='center')

    def vicsek_2(x, y, a, n):
        if n > 0:
            # Un carré
            case = draw.Rectangle(x - a/2, y - a/2, a, a)

            # pour en faire une animation ;-)
            case.appendAnim(draw.AnimateTransform('scale', '10s',
            '1, 1; 1.5, 1.5; 3, 3; 9, 9; 27, 27; 81, 81; 27, 27; 9, 9; 3, 3; 1.5, 1.5; 1, 1',
                                repeatCount='indefinite'))

            figure.append(case)

            a /= 3
            for dx, dy in [(-a, -a), (a, -a), (-a, a), (0, 0), (a, a)]:
                vicsek_2(x + dx, y + dy, a, n - 1)


    a = 400
    x, y = 0, 0     # un peu de marge
    n = 5           # profondeur de récursion

    vicsek_2(x, y, a, n)

    figure
    ```



