---
author: Franck CHAMBON
---

# 📐 Fractale cercle 4

**Sans modifier la fonction `motif` précédente**, construire la figure suivante :

![](./fractales/circle_4.svg){ .autolight }



On pourra compléter le code :

```python
import drawSvg as draw

d = draw.Drawing(...)  # ← à compléter


r = 100
x, y = ...  # ← à compléter
k = 0.4             # facteur de réduction des cercles
n = 8               # profondeur de récursion
dx, dy = ...  # ← à compléter
e = 3               # épaisseur du premier trait
motif(...)  # ← à compléter

d
```

??? done "Réponse"

    ```python
    import drawSvg as draw

    d = draw.Drawing(500, 500)

    r = 100
    x, y = 250, 250     # un peu de marge
    k = 0.4             # facteur de réduction des cercles
    n = 8               # profondeur de récursion
    dx, dy = 0, 0       # au début, on vient de nulle part !!!!
    e = 3               # épaisseur du premier trait
    motif(x, y, r, dx, dy, k, e, n)

    d
    ```


