---
author: Franck CHAMBON
---

# 📐 La tête à toto

!!! note "Exercice"
    Reproduire cette figure :

    ![](./fractales/toto.svg){ .autolight }

!!! tip "Indices"
    1. Le rapport de réduction dans la récursion est $0,\!35$, ainsi que pour l'épaisseur.
    2. Si le rayon du nez vaut 1 unité, alors
        - la bouche est de hauteur 1
        - les lèvres sont décalées de 2 et 3 unités sous le nez
        - les yeux sont décalés de 1.5 au-dessus du nez et de 2.5 de part et d'autre.
        - la tête a un rayon de 5 unités.
        - l'épaisseur du premier trait est $\frac14$ d'unité.
    3. Au niveau 0, on ne trace que le cercle de la tête.
    4. Amusez-vous bien pour le reste !

??? done "Réponse"

    ```python
    import drawSvg as draw

    figure = draw.Drawing(500, 500)

    def toto(x, y, r, e, n):
        """Dessine un motif,
            - avec un premier cercle (x, y, r)
            - un nez en +
            - une bouche en =
            - tout cela d'épaisseur e
            - deux yeux récursifs semblable au modèle,
            - facteur récursif ×0.35
        """
        # tête
        figure.append(draw.Circle(x, y, 5*r,
                fill='none', stroke_width=e, stroke='black'))

        if n > 0:
            # nez
            dh = r
            figure.append(draw.Lines(x + dh, y,      x - dh, y,
                stroke_width=e, stroke='black'))
            figure.append(draw.Lines(x,      y + dh, x,      y - dh,
                stroke_width=e, stroke='black'))

            # bouche
            dx = r
            dy = -3 * r
            dh = r
            figure.append(draw.Lines(x - dx, y + dy + dh, x + dx , y + dy + dh,
                stroke_width=e, stroke='black'))
            figure.append(draw.Lines(x - dx, y + dy     , x + dx , y + dy     ,
                stroke_width=e, stroke='black'))


            # yeux
            dx = 2.5 * r
            dy = 1.5 * r
            y_ = y + dy
            r_ = 0.35 * r
            e_ = 0.35 * e
            toto(x - dx, y_, r_, e_, n - 1)
            toto(x + dx, y_, r_, e_, n - 1)


    r = 44
    x, y = 250, 250     # un peu de marge
    n = 4               # profondeur de récursion
    e = 11              # épaisseur du premier trait
    toto(x, y, r, e, n)

    figure
    ```
