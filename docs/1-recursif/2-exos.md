---
author: Franck CHAMBON
---

# 🐻 Petits exercices

## Socle de statues

Sur France-IOI, reprendre l'exercice [Socle de statues](http://www.france-ioi.org/algo/task.php?idChapter=843&idTask=1934) avec une solution récursive

On pourra compéter la fonction récursive :

```python
def volume_socle(l_bas, l_haut):
    ...


assert volume_socle(7, 3) == 135

l_bas = int(input())
l_haut = int(input())
print(volume_socle(l_bas, l_haut))
```

??? done "Solution"

    === "Solution 1"

        Un socle est constitué :
        
        - d'une base de volume `l_bas * l_bas * 1`
        - et du reste de `volume_socle(l_bas - 1, l_haut)`

        ```python
        def volume_socle(l_bas, l_haut):
            "Volume d'un socle pour statues"
            if l_bas < l_haut:
                return 0
            else:
                volume_base = l_bas * l_bas * 1
                return volume_base + volume_socle(l_bas - 1, l_haut)
        ```

    === "Solution 2"

        Un socle est constitué :
        
        - d'un support de volume `l_haut * l_haut * 1`
        - et du reste de `volume_socle(l_bas, l_haut + 1)`

        ```python
        def volume_socle(l_bas, l_haut):
            "Volume d'un socle pour statues"
            if l_bas < l_haut:
                return 0
            else:
                volume_support = l_haut * l_haut * 1
                return volume_socle(l_bas, l_haut + 1) + volume_support
        ```

## Maximum d'une liste non vide

Écrire une fonction récursive `maxi` telle que `maxi(nombres)` renvoie le maximum de la liste `nombres` qui est non vide.


!!! tip "Indices"
    1. Penser à ajouter une docstring.
    2. Pour obtenir la copie d'une tranche du second élément jusqu'à la fin, on peut écrire `#!py nombres[1:]`
    3. Utiliser une variable pour stocker un résultat à utiliser plusieurs fois.
    4. Utiliser une fonction qui renvoie le maximum de deux nombres.

{{ IDE('maxi') }}


??? done "Réponse"

    ```python
    def maxi_2(a, b):
        if a > b:
            return a
        else:
            return b
    
    def maxi(nombres):
        "Renvoie le maximum de la liste non vide de nombres"
        debut = nombres[0]
        if len(nombres) == 1:
            return debut
        else:
            maxi_fin = maxi(nombres[1:])
            return maxi_2(debut, maxi_fin)


    assert maxi([7]) == 7
    assert maxi([7, 13]) == 13
    assert maxi([13, 7]) == 13
    assert maxi([-19, -13, -19]) == -13
    ```


## Test de palindrome

Palindrome
: Un mot est un palindrome s'il se lit de la même façon de droite à gauche comme de gauche à droite.

Écrire une fonction récursive telle que `est_palindrome(mot)` renvoie un booléen qui détermine si la chaine `mot` est un palindrome.


!!! tip "Indices"

    1. Penser à ajouter une docstring et d'autres tests unitaires.
    2. Le premier caractère de la chaine `mot` est `#!py mot[0]`
    3. Le dernier caractère de la chaine `mot` est `#!py mot[-1]`
    4. Pour une copie d'une tranche du second jusqu'à l'avant-dernier caractère d'une chaine `mot`, on peut écrire `#!py mot[1:-1]`

{{ IDE('palindrome') }}


??? done "Réponse"

    ```python
    def est_palindrome(mot):
        "Détermine si mot est un palindrome"

        if len(mot) < 2:
            return True
        else:
            return (mot[0] == mot[-1]) and est_palindrome(mot[1:-1])

        
    assert est_palindrome('') == True, "Mot vide"
    assert est_palindrome('m') == True, "Mot d'une lettre"
    assert est_palindrome('mm') == True, "Deux lettres identiques"
    assert est_palindrome('mo') == False, "Deux lettres distinctes"
    assert est_palindrome('mot') == False, "Trois lettres"
    assert est_palindrome('tot') == True, "Trois lettres"
    assert est_palindrome('esse') == True, "Quatre lettres OK"
    assert est_palindrome('test') == False
    assert est_palindrome('esst') == False
    assert est_palindrome('esses') == False
    assert est_palindrome('kayak') == True
    ```

    Il est bon d'ajouter de nombreux tests de natures très différentes.

    :warning: Cette solution à base de copie de tranches n'est pas efficace, nous verrons ensuite une méthode plus efficace avec les indices.

## Compte d'occurrences

Écrire une fonction récursive telle que `nb_occurrences(lettre, mot)` renvoie le nombre d'occurrences de la `lettre` dans le `mot`.

!!! tip "Indice"
    1. Penser à ajouter une docstring et des tests unitaires.

{{ IDE('nb_occurences') }}

??? done "Réponse"

    ```python
    def nb_occurrences(lettre, mot):
        "Renvoie le nombre d'occurrences de la lettre dans le mot"
        if len(mot) == 0:
            return 0
        else:
            nb_occ = nb_occurences(lettre, mot[1:])
            if lettre == mot[0]:
                nb_occ += 1
            return nb_occ
    
    assert nb_occurrences("o", "bonjour") == 2
    assert nb_occurrences("i", "salut") == 0
    assert nb_occurrences("t", "tttt") == 4
    ```


## Triangle en ASCII

Écrire deux fonctions récursives `triangle_bas`, puis `triangle_haut` prenant un entier `n` en paramètre et qui **affichent** un triangle.

```pycon
>>> triangle_bas(4)  # affiche un triangle tête en bas
####
###
##
#
```

```pycon
>>> triangle_haut(4)  # affiche un triangle tête en haut
#
##
###
####
```

{{ IDE() }}

??? done "Solution"

    ```python
    def triangle_bas(n):
        "Affiche un triangle tête en bas"
        if n > 0:
            print('#' * n)
            triangle_bas(n - 1)
    
    def triangle_haut(n):
        "Affiche un triangle tête en haut"
        if n > 0:
            triangle_haut(n - 1)
            print('#' * n)
    ```

    L'ordre des instructions est bien sûr important !


## Nombre encadré

Écrire une fonction récursive `encadre` qui permette d'obtenir `[[[42]]]` par exemple, avec comme paramètres : le `contenu` et l'`epaisseur` de l'encadrement.

- Allez-vous utiliser `#!py print` ou `#!py return` ?
- Quels seront les types de vos paramètres, et de la sortie ?

!!! tip "Indice"
    Pour afficher un `texte` sans aller à la ligne, on peut écrire `#!py print(texte, end="")`

{{ IDE() }}

??? done "Réponse"

    === "Avec `#!py print`"

        ```python
        def encadre(contenu: str, epaisseur: int) -> None:
            "Affiche le contenu avec une epaisseur de []"
            if epaisseur == 0:
                print(contenu, end="")
            else:
                print("[", end="")
                encadre(contenu, epaisseur - 1)
                print("]", end="")
        ```

        - Cette solution est correcte,
        - elle peut s'utiliser avec `#!py encadre(42, 3)` ou mieux avec `#!py encadre("42", 3)`
        - mais il n'y a pas de retour à la ligne une fois fini,
        - et surtout, on ne peut pas réutiliser le résultat affiché dans une autre fonction.

        Il vaut mieux une autre solution, qui renvoie le texte, que l'on affichera à part !

        Bonne pratique : observez l'annotation des types, dans les cas simples, il faut se forcer à l'utiliser !

    === "Avec `#!py return` ; V1"

        ```python
        def encadre(contenu: str, epaisseur: int) -> str:
            "Renvoie le contenu encadré d'une épaisseur de []"
            if epaisseur == 0:
                return contenu
            else:
                return "[" + encadre(contenu, epaisseur - 1) + "]"
        ```

        - Meilleure solution,
        - le résultat peut être remobilisé ou être affiché. Exemple :

        ```pycon
        >>> encadre("On peut utiliser " + encadre("la fonction encadre", 3)+ " de manière plus complexe.", 2)
        [[On peut utiliser [[[la fonction encadre]]] de manière plus complexe.]]
        ```

        Il n'était pas possible de faire cela avec la version `#!py print`.

    === "Avec `#!py return` ; V2"

        ```python
        def encadre(contenu: str, epaisseur: int) -> str:
            "Renvoie le contenu encadré d'une épaisseur de []"
            if epaisseur == 0:
                return contenu
            else:
                return encadre("[" + contenu + "]", epaisseur - 1)
        ```

        Variante correcte avec une différence fondamentale :

        - en dehors du cas de base, l'appel récursif est **la dernière instruction** de la fonction,
        - on appelle cette technique **fonction récursive terminale**,
        - Python n'en tire pas profit, et ne le fera jamais,
        - mais d'autres langages de programmation en tirent profit,
        - ils transforment le code automatiquement en code itératif qui peut consommer **beaucoup** moins de mémoire,
        - ceci est étudié en post BAC.


## La tête à Toto

Écrire une fonction récursive telle que :

- `toto(0)` renvoie `'0'`
- `toto(1)` renvoie `'(0 + 0)'`
- `toto(2)` renvoie `'((0 + 0) + (0 + 0))'`
- etc sur le même principe

!!! tip "Indice"
    1. Penser à ajouter une docstring et des tests unitaires.
    2. La concaténation de chaines de caractères `motif_1` et `motif_2` se fait avec `#!py motif_1 + motif_2`

{{ IDE('toto') }}


??? done "Réponse"

    ```python
    def toto(n):
        """Une fonction telle que 
            pour n un entier positif
            on a :
        
        >>> toto(0)
        '0'
        >>> toto(1)
        '(0 + 0)'
        >>> toto(2)
        '((0 + 0) + (0 + 0))'

        Etc sur le même principe.
        """

        if n == 0:
            return '0'
        else:
            motif = toto(n - 1)
            return '(' + motif + ' + ' + motif + ')'
        

    assert toto(0) == '0'
    assert toto(1) == '(0 + 0)'
    assert toto(2) == '((0 + 0) + (0 + 0))'
    ```

    :warning: Notez comment les tests sont inclus dans la docstring, c'est une très bonne pratique. Cela permet d'automatiser les tests, avec `doctest`.

## Suppression de doublons consécutifs

:warning: Exercice difficile :warning:

Écrire une fonction récursive `nettoie` qui renvoie une copie de son paramètre `texte` où l'on a supprimé les caractères consécutifs identiques, en répétant l'action si nécessaire.

```pycon
>>> nettoie('bbcdaadz')
'cz'
```

En effet :

- on peut supprimer `bb`, ce qui donne `cdaadz`
- on peut supprimer `aa`, ce qui donne `cddz`
- on peut supprimer `dd`, ce qui donne `cz`



!!! tip "Indices"
    Il faudrait envisager trois cas distincts :

    1. le `texte` fait moins de deux caractères,
    2. le `texte` commence par deux caractères identiques,
    3. le `texte` commence par deux caractères différents.

    Pensez à ajouter docstring et de **nombreux** tests unitaires !


{{ IDE() }}


??? done "Réponse"

    ```python
    def nettoie(texte):
        "Renvoie le texte nettoyé suivant la règle donnée"
        if len(texte) < 2:
            return texte
        elif texte[0] == texte[1]:
            return nettoie(texte[2:])
        else:
            # Il y a deux caractères distincts au début
            suite = nettoie(texte[1:])
            if len(suite) > 0 and texte[0] == suite[0]:
                return suite[1:]
            else:
                return texte[0] + suite
    ```

    - Analysez tranquillement cette solution, plusieurs fois !!!
    - Attardons-nous ensuite sur la ligne suivante :
        - `#!py if len(suite) > 0 and texte[0] == suite[0]:`
        - il y a une évaluation paresseuse du `and`,
        - si `#!py len(suite) > 0` est faux, la suite du `and` n'est pas testé (c'est inutile), et aucune erreur n'est provoquée avec `#!py suite[0]` (pourtant `suite` est vide) !!!
    - Analysez encore cette solution !!!
    



## Le triangle de Pascal

Écrire une fonction récursive telle que `pascal(n)` renvoie la liste des `n + 1` premières lignes du triangle de Pascal.

!!! info "Règles"
    1. Le sommet est constitué du nombre $1$.
    2. Chaque ligne commence et finit par $1$.
    3. Chaque ligne, à partir de la deuxième, possède un élément de plus que la précédente.
    3. Chaque autre nombre est la somme des deux nombres situés juste au-dessus.

!!! tip "Indices"
    1. Pour créer une liste, on peut écrire `#!py [2, 3, 5, 7]`
    2. Pour ajouter un `element` à une `ligne`, on peut écrire `#!py ligne.append(element)`
    3. Pour ajouter une `ligne` à un `triangle` de lignes, on peut écrire `#!py triangle.append(ligne)`
    4. Pour obtenir la dernière ligne d'un `triangle` non vide de lignes, on peut écrire `#!py ligne = triangle[-1]`

{{ IDE('pascal') }}


??? done "Réponse"

    ```python
    def pascal(n):
        "renvoie la liste des n + 1 premières lignes du triangle de Pascal"
        if n == 0:
            return [[1]]
        else:
            triangle = pascal(n - 1)
            base = triangle[-1]

            derniere_ligne = [1]
            for i in range(1, n):
                derniere_ligne.append(base[i - 1] + base[i])
            derniere_ligne.append(1)

            triangle.append(derniere_ligne)
            return triangle


    assert pascal(4) == [
        [1],
        [1, 1],
        [1, 2, 1],
        [1, 3, 3, 1],
        [1, 4, 6, 4, 1],
    ]
    ```

## Triangle de Sierpiński

Le triangle de Sierpiński[^sierpinski] est un exemple simple de fractale :

[^sierpinski]: :material-wikipedia: [Triangle de Sierpiński](https://fr.wikipedia.org/wiki/Triangle_de_Sierpi%C5%84ski)

![](images/triangle_sierpinski.png){ .autolight }

1. On part d'un carré de côté 1.
2. On colle deux copies, une à droite, une en bas ; on obtient un triangle de pixels.
3. On colle deux copies de triangle, une à droite, une en bas.
4. On répète le processus de construction.
5. La largeur et la hauteur de chaque étape est une puissance de deux.

![](images/triangle_sierpinski2.png){ .autolight }

Écrire une fonction récursive telle que `sierpinski(h)` renvoie une liste de `h` lignes de pixels formant un triangle de Sierpiński. `h` sera un entier, une puissance de deux.

{{ IDE('sierpinski') }}

??? done "Réponse"
    Le cas de base étant écrit, on réfléchit au cas général :

    1. On demande par récursivité la `base` de hauteur `h = n // 2`.
    2. On construit `h` premières lignes avec une ligne de la base, des espaces, et une autre copie de la ligne de base. Il y a 0 espaces au premier tour, puis 1, puis 2, ...
    3. On construit les `h` lignes suivantes, simplement en ajoutant les lignes de la base.

    ```python
    def sierpinski(n):
        "Renvoie une liste de ligne, un triangle de Sierpiński de hauteur n"
        if n == 1:
            return ["#"]
        else:
            h = n // 2
            base = sierpinski(h)
            triangle = []
            for i in range(h):
                triangle.append(base[i] + " " * i + base[i])
            for i in range(h):
                triangle.append(base[i])
            return triangle
    ```


## Fonction 91 de McCarthy

La **fonction 91 de McCarthy**[^f91] est une fonction récursive définie pour $n \in \mathbb N$ par :

[^f91]: :material-wikipedia: [La fonction 91 de McCarthy](https://fr.wikipedia.org/wiki/Fonction_91_de_McCarthy)

$$f(n) =
\begin{cases}
n - 10 & {\rm {\ si\ }} n > 100 \\
f(f(n+11)) & {\rm {\ sinon.}}
\end{cases}$$

Écrire un script qui vérifie que cette fonction est constante, égale à $91$, pour $n \leqslant 100$.

{{ IDE() }}

??? done "Réponse"

    ```python
    def f(n):
        if n > 100:
            return n - 10
        else:
            return f(f(n + 11))

    for n in range(101):
        assert f(n) == 91
    ```
