def est_palindrome(mot):
    ...
    
    
assert est_palindrome('mot') == False
assert est_palindrome('tot') == True
assert est_palindrome('esse') == True
