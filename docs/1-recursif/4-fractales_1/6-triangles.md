---
author: Franck CHAMBON
---

# 📐 Triangle de triangles

!!! info "Dessiner un triangle"

    ![](./images_rec/1_triangle.svg){ .autolight }

    ```python
    figure.append(draw.Lines(x_a, y_a, x_b, y_b, x_c, y_c,
        close=True, fill='none'))
    ```

    À une `figure`, ajoute un triangle $ABC$ dont les sommets ont pour coordonnées : `(x_a, y_a), (x_b, y_b), (x_c, y_c)`

    1. On peut dessiner une ligne polygonale avec 2, 3, 4 ou plus de points au lieu de 3.
    2. Cette ligne peut être fermée (`#!py close=True`) ou non (`#!py close=False`).
    3. Le remplissage peut être creux (`#!py fill='none'` ; du texte sans majuscule !) ou bien utiliser une couleur donnée sous forme RGB (`#!py fill='#1248ff'` ; un bleu ciel) ou avec un nom HTML (`#!py fill='lime'` ; jaune citron)
    4. On peut utiliser d'autres couples clé-valeur, exemples :
        - la couleur du trait, `#!py stroke='red'`
        - l'épaisseur du trait, `#!py stroke_width=2.5`
        - la transparence du remplissage, `#!py fill_opacity=0.2`



!!! note "Exercice"
    Construire cette figure avec des fonctions récursives, sans jamais écrire de boucle `#!py for` ni de boucle `#!py while`.

    ![](./images_rec/triangles.svg){ .autolight }


!!! tip "Indices"
    1. La fonction qui dessine **un** triangle équilatéral, tête en haut vous est donné. Analysez-la !
    2. On pourra commencer par écrire une fonction qui dessine `n` triangles équilatéraux, les uns à côtés des autres horizontalement, tête en haut, tous de côté `a`, avec le sommet tout à gauche de coordonnées : `(x, y)`.
    3. On pourra alors écrire une fonction qui dessine tous les triangles.
    4. On pourra compléter le code suivant :

    ```python
    import drawSvg as draw
    from math import sqrt

    LARGEUR, HAUTEUR = 550, 500
    figure = draw.Drawing(LARGEUR, HAUTEUR)


    def triangle_equilateral(a, x, y):
        """Dessine un triangle équilatéral de côté a
        tête en haut,
        le sommet en bas à gauche est en (x, y)
        """
        h = a * sqrt(3) / 2
        figure.append(draw.Lines(x, y, x+a, y, x + a/2, y + h,
            close=True, fill='none', stroke='red', stroke_width=3))

    def ligne_triangle(n, a, x, y):
        """Dessine une ligne de n triangles de côtés a,
        horizontalement, les uns à côtés des autres,
        le sommet en bas, le plus à gauche est en (x, y)
        """
        ...  # ← à compléter


    def triangle_de_triangles(n, a, x, y):
        """Dessine une pyramide de triangles, de côté n×a,
        avec le coin en bas à gauche en (x, y)
        """
        ...  # ← à compléter

    triangle_de_triangles(5, 100, 25, 25)

    figure
    ```

??? done "Réponse"

    ```python
    import drawSvg as draw
    from math import sqrt

    figure = draw.Drawing(550, 500)


    def triangle_equilateral(a, x, y):
        """Dessine un triangle équilatéral de côté a
        tête en haut,
        le sommet en bas à gauche est en (x, y)
        """
        h = a * sqrt(3) / 2
        figure.append(draw.Lines(x, y, x+a, y, x + a/2, y + h,
            close=True, fill='none', stroke='red', stroke_width=3))

    def ligne_triangle(n, a, x, y):
        """Dessine une ligne de n triangles de côtés a,
        horizontalement, les uns à côtés des autres,
        le sommet en bas, le plus à gauche est en (x, y)
        """
        if n > 0:
            triangle_equilateral(a, x, y)
            ligne_triangle(n - 1, a, x + a, y)


    def triangle_de_triangles(n, a, x, y):
        """Dessine une pyramide de triangles, de côté n×a,
        avec le coin en bas à gauche en (x, y)
        """
        if n > 0:
            ligne_triangle(n, a, x, y)
            h = a * sqrt(3) / 2
            triangle_de_triangles(n - 1, a, x + a/2, y + h)

    triangle_de_triangles(5, 100, 25, 25)

    figure
    ```

