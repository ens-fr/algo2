import drawSvg as draw
from math import sqrt

d = draw.Drawing(550, 500)


def triangle_equilateral(a, x, y):
    """Dessine un triangle équilatéral de côté a
    tête en haut,
    le sommet en bas à gauche est en (x, y)
    """
    h = a * sqrt(3) / 2
    d.append(draw.Lines(x, y, x+a, y, x + a/2, y + h,
        close=True, fill='none', stroke='black', stroke_width=3))

def ligne_triangle(n, a, x, y):
    """Dessine une ligne de n triangles de côtés a,
    horizontalement, les uns à côtés des autres,
    le sommet en bas, le plus à gauche est en (x, y)
    """
    if n > 0:
        triangle_equilateral(a, x, y)
        ligne_triangle(n - 1, a, x + a, y)


def motif(n, a, x, y):
    """Dessine une pyramide de triangles, de côté n×a,
    avec le coin en bas à gauche en (x, y)
    """
    if n > 0:
        ligne_triangle(n, a, x, y)
        h = a * sqrt(3) / 2
        motif(n - 1, a, x + a/2, y + h)

motif(5, 100, 25, 25)

d.saveSvg('triangles.svg')
