from svg_turtle import SvgTurtle
# pour des sorties Tortues SVG

t = SvgTurtle(500, 250)

def motif(x, n):
    if n > 0:
        for _ in range(4):
            t.forward(x)
            t.left(90)
        t.forward(x)
        motif(x / 2, n - 1)

t.penup()
t.goto(-200, -100)
t.pendown()
t.pensize(3)
t.color('black')
motif(200, 10)
t.save_as('squares.svg')
