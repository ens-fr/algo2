import drawSvg as draw

d = draw.Drawing(410, 410)

def motif(x, y, r, k, n):
    if n > 0:
        d.append(draw.Circle(x, y, r,
                fill='none', stroke_width=2, stroke='black'))

        x_ = x + r + k*r  # décalage à droite
        y_ = y + r + k*r  # décalage en haut
        r *= k
        motif(x_, y, r, k, n - 1)
        motif(x, y_, r, k, n - 1)

r = 100
x, y = r + 5, r + 5  # un peu de marge
k = 1/2
n = 10  # profondeur de récursion
motif(x, y, r, k, n)

d.saveSvg('circle_2.svg')
