from random import *
from time import *

tas = [(randint(36, 46), bool(randint(0, 1))) for _ in range(10**7)]

def f1(L):
    t = [0]*50
    for x, p in L:
        if p:
            t[x] += 1
        else:
            t[x] -= 1

def f2(L):
    t = {i:0 for i in range(36, 47)}
    for x, p in L:
        if p:
            t[x] += 1
        else:
            t[x] -= 1


t1 = time()
f1(tas)
t2 = time()
f2(tas)
t3 = time()

print(t2-t1)
print(t3-t2)
