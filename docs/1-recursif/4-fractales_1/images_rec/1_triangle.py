import drawSvg as draw

d = draw.Drawing(200, 200)


x_a, y_a = 15, 20
x_b, y_b = 180, 50
x_c, y_c = 80, 180

d.append(draw.Lines(x_a, y_a, x_b, y_b, x_c, y_c,
    close=True, fill='none', stroke='black', stroke_width=3))

d.saveSvg('1_triangle.svg')
