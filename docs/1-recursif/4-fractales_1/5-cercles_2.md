---
author: Franck CHAMBON
---

# 📐 Fractale cercle 2

!!! note "Exercice"
    Construire la figure suivante :

    ![](./images_rec/circle_2.svg){ .autolight }

    Il y a 10 tailles de cercle, chacun (sauf le plus gros) est 2 fois plus petit que son voisin de gauche ou du bas.

!!! tip "Indice"
    On reprendra le script vu avec « Fractale cercle 1 » et on l'adaptera.

??? done "Réponse"

    ```python
    import drawSvg as draw

    LARGEUR, HAUTEUR = 410, 210
    figure = draw.Drawing(LARGEUR, HAUTEUR)

    def cercle(x, y, r):
        "dessine un cercle sur la figure"
        figure.append(draw.Circle(x, y, r,
                    fill='none',
                    stroke_width=2,
                    stroke='black',
        ))


    def cercles_2(x, y, r, k, n):
        if n > 0:
            cercle(x, y, r)
            x_ = x + r + k*r  # décalage à droite
            y_ = y + r + k*r  # décalage en haut
            r *= k
            cercles_2(x_, y , r, k, n - 1)
            cercles_2(x , y_, r, k, n - 1)
    ```

