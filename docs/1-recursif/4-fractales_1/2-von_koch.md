---
author: Franck CHAMBON
---

# 🐢 Von Koch

Avec la courbe de Koch, on pourra tracer le célèbre flocon.

Concentrons-nous sur la courbe dans un premier temps.

## Courbe de Koch

La courbe de Koch est d'une fractale qui est composée de 4 copies d'elle-même réduites d'un facteur $\frac13$ et disposée avec des angles de 60°, comme sur la figure ci-dessous :

![](./fractales/von_koch_intro.svg){ .autolight }

Cette figure, avec ces arcs de cercles en guise d'aide, sera utile pour écrire une fonction telle que `koch(d, n)` trace une approximation de cette fractale de niveau `n` en déplaçant la tortue globalement d'une distance `d`.

!!! note "Question 1"
    L'objectif étant de réaliser la figure suivante :

    ![](./fractales/von_koch.svg){ .autolight }

    Compléter le code suivant :

    ```python
    import turtle

    def koch(d: float, n: int):
        """Dessine la courbe de Koch, vers la gauche,
        sur une distance `d` à vol d'oiseau,
        la tortue est décalée à la fin de la courbe,
        elle conserve son orientation initiale.
        n : profondeur de récursion ;
          avec n = 0, trace un trait droit.
        """
        ... # À compléter ici

    turtle.speed(1)  # de 1 (lent) à 11 (rapide)
    koch(100.0, 6)
    turtle.done()
    ```

??? tip "Indice 1"
    - Le cas `n == 0` doit être le seul à comporter l'instruction `turtle.forward`.
    - Le cas général doit comporter **4 appels** récursifs.
    - Vérifier les rotations de la tortue en testant votre code à la main avec un stylo, par exemple. Ainsi que sur la figure tout en haut.
    - Vous pouvez modifier la vitesse de la tortue.

??? done "Réponse"

    ```python
    def koch(d: float, n: int):
        """Dessine la courbe de Koch, vers la gauche,
        sur une distance `d` à vol d'oiseau,
        la tortue est décalée à la fin de la courbe,
        elle conserve son orientation initiale.
        n : profondeur de récursion
          avec n = 0, trace un trait droit.
        """
        if n == 0:
            turtle.forward(d)
        else:
            x = d / 3
            koch(x, n - 1)
            turtle.left(60)
            koch(x, n - 1)
            turtle.right(120)
            koch(x, n - 1)
            turtle.left(60)
            koch(x, n - 1)
    ```

!!! note "Question 2"
    Factoriser ce code, avec une boucle qui fait 4 tours, un pour chaque `koch(x, n - 1)`

??? tip "Indice 2"
    - On pourra remplacer `right(120)` par `left(-120)`.
    - On pourra ajouter `left(0)` à la fin,
    - ce qui donne 4 fois deux lignes proches.

??? done "Réponse"

    ```python
    def koch(d: float, n: int):
        """Dessine la courbe de Koch, vers la gauche,
        sur une distance `d` à vol d'oiseau,
        la tortue est décalée à la fin de la courbe,
        elle conserve son orientation initiale.
        n : profondeur de récursion
          avec n = 0, trace un trait droit.
        """
        if n == 0:
            turtle.forward(d)
        else:
            x = d / 3
            for angle in [60, -120, 60, 0]:
                koch(x, n - 1)
                turtle.left(angle)
    ```

    On peut aussi écrire

    ```python
            x = d / 3
            m = n - 1
            for k in [+1, -2, +1, 0]:
                koch(x, m)
                turtle.left(k * 60)
    ```

## Flocon de Von Koch

!!! note "Question 3"
    Objectif : dessiner un flocon de Von Koch, à l'aide de la figure ci-dessous.

    ![](./fractales/flocon.svg){ .autolight }

    Réutiliser la fonction `koch` sans la modifier, et compléter le code suivant :

    ```python
    import turtle

    def koch(d: float, n: int):
        ...  # à recopier

    def flocon(d: float, n: int):
        """Dessine un flocon de Von Koch,
        en commençant par une courbe de Von Koch,
        puis deux autres pour former une boucle.
        La tortue retrouve sa position et orientation initiale à la fin.
        """
        ...  # À compléter


    turtle.speed(11)  # de 1 (lent) à 11 (rapide)
    turtle.penup()
    turtle.backward(100)
    turtle.pendown()

    flocon(150, 5)

    turtle.done()
    ```

??? done "Réponse"

    ```python
    def flocon(d: float, n: int):
        """Dessine un flocon de Von Koch,
        en commençant par une courbe de Von Koch,
        puis deux autres pour former une boucle en tournant à droite.
        La tortue retrouve sa position et orientation initiale à la fin.
        """
        for _ in range(3):
            koch(d, n)
            turtle.right(120)
    ```
