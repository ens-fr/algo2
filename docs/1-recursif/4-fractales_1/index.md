---
author: Franck CHAMBON
---

# ❗ Introduction

!!! info "Avant de commencer"
    Avez-vous découvert [le dessin avec Python](../../1-Python/7-dessiner.md){ .md-button } ?

    Un chapitre dédié aux premières utilisations de 
    
    - 🐢 `turtle`,
    - 📐 `drawSvg`.

Nous apprendrons ici à dessiner des figures récursives.

