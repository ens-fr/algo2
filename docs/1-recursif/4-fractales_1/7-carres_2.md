---
author: Franck CHAMBON
---

# 📐 Fractale avec des carrés

**Challenge**, construire la figure suivante :

![](./fractales/square_4.svg){ .autolight }

!!! tip "Indice"
    Pour dessiner un carré de côté `a` et de centre `(x, y)`, on pourra faire

    ```python
    draw.Rectangle(x - a/2, y - a/2, a, a,
        stroke='black', stroke_width=a/16,fill='none')
    ```

??? done "Réponse"

    ```python
    import drawSvg as draw

    figure = draw.Drawing(500, 500, origin='center')

    def square_4(x, y, a, n):
        if n > 0:
            figure.append(draw.Rectangle(x - a/2, y - a/2, a, a,
                stroke='black', stroke_width=a/16,fill='none'))
            a /= 2
            for dx, dy in [(-a, -a), (a, -a), (-a, a), (a, a)]:
                square_4(x + dx, y + dy, a, n - 1)


    a = 200
    x, y = 0, 0
    n = 4           # profondeur de récursion

    square_4(x, y, a, n)

    figure
    ```

