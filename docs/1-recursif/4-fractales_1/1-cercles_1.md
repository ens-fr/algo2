---
author: Franck CHAMBON
---

# 📐 Fractale cercle 1

Construire la figure suivante (l'approximation d'une fractale) :

![](./images_rec/circle_1.svg){ .autolight }

- Il y a 10 cercles, chacun est 2 fois plus petit que son voisin de gauche.

On complètera le script suivant où `k = 1/2` est le coefficient de réduction d'un cercle au suivant et `n = 10` est le nombre de cercles.


```python
import drawSvg as draw

LARGEUR, HAUTEUR = 410, 210
figure = draw.Drawing(LARGEUR, HAUTEUR)

def cercle(x, y, r):
    "dessine un cercle sur la figure"
    figure.append(draw.Circle(x, y, r,
                fill='none',
                stroke_width=2,
                stroke='black',
    ))


def cercles_1(x, y, r, k, n):
    """Dessine n cercles,
    - le premier de centre (x, y) et de rayon r
    - le suivant collé à droite, avec une réduction de facteur k
    - etc
    """
    if n > 0:
        cercle(..., ..., ...)
        ...

r = 100
x, y = r + 5, r + 5  # un peu de marge
k = 1/2  # coefficient de réduction
n = 10   # profondeur de récursion
cercles_1(x, y, r, k, n)

figure.saveSvg('cercles_1.svg')
figure  # pour affichage dans Jupyter
```

??? tip "Indices"
    1. Le cercle à droite d'un cercle de rayon `r` aura un rayon de `k * r`
    2. Le centre du cercle suivant est décalé à droite de `r` **et** de `k * r`

??? done "Réponse"

    ```python
    def cercles_1(x, y, r, k, n):
        if n > 0:
            cercle(x, y, r)
            x += r + k*r  # décalage à droite
            r *= k
            cercles_1(x, y, r, k, n - 1)
    ```
