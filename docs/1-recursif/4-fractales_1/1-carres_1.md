---
author: Franck CHAMBON
---

# 🐢 Lot de carrés

Exercice : Construire avec [Basthon](https://console.basthon.fr) une image de cette allure :

![](images_rec/squares.svg){ .autolight }

Il y a 10 carrés, chacun est deux fois plus petit que son voisin de gauche.

On pourra compléter le code :

```python
import turtle

def des_carres(x, n):
    """Dessine n carrés dont le premier est de taille x
    le suivant à droite est deux fois plus petit...
    """
    ...

turtle.penup()
turtle.goto(-200, -100)
turtle.pendown()
turtle.pensize(3)

des_carres(200, 10)

turtle.done()
```

??? tip "Indice"

    On pourra commencer par écrire une fonction `carre`

    ```python
    def carre(x):
        "Dessine un carré de côte x"
        for _ in range(4):
            turtle.forward(x)
            turtle.left(90)
    ```

??? done "Réponse"

    === "Solution itérative"

        ```python
        def des_carres(x, n):
            """Dessine n carrés dont le premier est de taille x
            le suivant à droite est deux fois plus petit...
            """
            for i in range(n):
                carre(x)
                turtle.forward(x)
                x = x / 2
        ```

        La solution itérative était simple à écrire

        Essayez d'en faire une récursive avant de voir la solution.

    === "Solution récursive"

        ```python
        def des_carres(x, n):
            """Dessine n carrés dont le premier est de taille x
            le suivant à droite est deux fois plus petit...
            """
            if n > 0:
                carre(x)
                turtle.forward(x)
                des_carres(x / 2, n - 1)
        ```

        Ici, la solution récursive ne constitue qu'un entrainement pour la suite !
