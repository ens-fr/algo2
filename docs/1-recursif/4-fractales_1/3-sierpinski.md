---
author: Franck CHAMBON
---

# 🐢 Triangle de Sierpiński

Étudiez le code suivant et le résultat produit

```python
import turtle

def motif(l, n):
    if n > 0:
        for _ in range(4):
            motif(l / 3, n - 1)
            turtle.forward(l)
            turtle.left(90)
        
turtle.speed(1)  # tortue lente
motif(200, 5)
turtle.done()
```

![](./fractales/sierpinski_var.svg){ .autolight }

!!! note "Question"
    Modifiez le script précédent pour obtenir une fonction `sierpinski` avec les résultats suivants

    === "Exemple, `n = 0`"
        ![](./fractales/sierpinski_0.svg){ .autolight }

        Une image vide, oui !

        ```python
        sierpinski(200, 0)
        ```
    === "Exemple, `n = 1`"
        ![](./fractales/sierpinski_1.svg){ .autolight }

        Un triangle équilatéral seul !

        ```python
        sierpinski(200, 1)
        ```
    === "Exemple, `n = 2`"
        ![](./fractales/sierpinski_2.svg){ .autolight }

        Le début d'une forme

        ```python
        sierpinski(200, 2)
        ```
    === "Exemple, `n = 3`"
        ![](./fractales/sierpinski_3.svg){ .autolight }

        Le début des jolies choses

        ```python
        sierpinski(200, 3)
        ```
    === "Exemple, `n = 4`"
        ![](./fractales/sierpinski_4.svg){ .autolight }

        Le début des jolies choses

        ```python
        sierpinski(200, 4)
        ```
    === "Exemple, `n = 5`"
        ![](./fractales/sierpinski_5.svg){ .autolight }

        Un jolie fractale en devenir

        ```python
        sierpinski(200, 5)
        ```


??? done "Réponse"

    ```python
    import turtle

    def sierpinski(l, n):
        "Dessine le triangle de Sierpiński"
        if n > 0:
            for _ in range(3):
                sierpinski(l / 2, n - 1)
                turtle.forward(l)
                turtle.left(120)
            
    turtle.speed(11)  # tortue très rapide
    sierpinski(200, 5)
    turtle.done()
    ```
