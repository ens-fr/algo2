from svg_turtle import SvgTurtle
turtle = SvgTurtle(550, 550)

def sierpinski(l, n):
    if n > 0:
        for _ in range(4):
            sierpinski(l / 3, n - 1)
            turtle.forward(l)
            turtle.left(90)
        
turtle.penup()
turtle.goto(-250, -250)
turtle.pendown()
sierpinski(500, 5)

turtle.save_as('sierpinski_var.svg')

