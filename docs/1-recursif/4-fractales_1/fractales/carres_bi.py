from svg_turtle import SvgTurtle
turtle = SvgTurtle(700, 550)

from math import sqrt

def carre_plein(l, couleur):
    "Dessine un carré de côté l, rempli avec couleur"
    turtle.fillcolor(couleur)
    turtle.begin_fill()
    for _ in range(4):
        turtle.forward(l)
        turtle.left(90)
    turtle.end_fill()
    

def carres_bi(l, est_rouge, n):
    if n > 0:
        turtle.pencolor('black')
        turtle.pensize(l * 0.05)
        if est_rouge:
            couleur = 'red'
        else:
            couleur = 'blue'
        
        carre_plein(l, couleur)
        turtle.penup()
        turtle.forward(l / 2)
        turtle.left(45)
        turtle.pendown()
        carres_bi(l / sqrt(2), not est_rouge, n - 1)

turtle.speed(11)
turtle.goto(-200, -200)
carres_bi(400, True, 10)
turtle.save_as('carre_bi.svg')

