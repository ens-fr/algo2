from svg_turtle import SvgTurtle
turtle = SvgTurtle(700, 550)

def branche(l, n):
    a = 30
    if n < 1:
        return
    if n == 1:
        turtle.pensize(l * 0.05)
        turtle.forward(l)
        turtle.backward(l)
        return
    l /= 2
    branche(l, n-2)
    turtle.pensize(l * 0.05)
    turtle.forward(l)
    turtle.left(a)
    branche(l, n-1)
    turtle.right(2*a)
    branche(l, n-1)
    turtle.left(a)
    branche(l, n-2)
    turtle.backward(l)


turtle.left(90)
for _ in range(6):
    branche(200, 6)
    turtle.left(60)

turtle.save_as('flocon_2.svg')
