import drawSvg as draw

d = draw.Drawing(500, 500, origin='center')

def square_4(x, y, a, n):
    if n > 0:
        d.append(draw.Rectangle(x - a/2, y - a/2, a, a,
            stroke='black', stroke_width=a/16,fill='none'))
        a /= 2
        for dx, dy in [(-a, -a), (a, -a), (-a, a), (a, a)]:
            square_4(x + dx, y + dy, a, n - 1)


a = 200
x, y = 0, 0
n = 4           # profondeur de récursion

square_4(x, y, a, n)

d.saveSvg('square_4.svg')
