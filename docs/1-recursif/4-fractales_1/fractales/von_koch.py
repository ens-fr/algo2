from svg_turtle import SvgTurtle
turtle = SvgTurtle(450, 150)

def koch(a: float, n: int):
    """Dessine la courbe de Koch, vers la gauche,
    sur une distance `a` à vol d'oiseau,
    la tortue est décalée à la fin de la courbe,
    elle conserve son orientation initiale.
    n : profondeur de récursion
    """
    if n == 0:
        turtle.forward(a)
    else:
        b = a / 3
        koch(b, n - 1)
        turtle.left(60)
        koch(b, n - 1)
        turtle.right(120)
        koch(b, n - 1)
        turtle.left(60)
        koch(b, n - 1)

turtle.speed(11)  # de 1 (lent) à 11 (rapide)
turtle.penup()
turtle.goto(-200, -55)
turtle.pendown()
koch(400.0, 6)
turtle.save_as('von_koch.svg')
