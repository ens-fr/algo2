from svg_turtle import SvgTurtle


for n in range(6):
    turtle = SvgTurtle(550, 500)
    turtle.pensize(3)

    def sierpinski(l, n):
        if n > 0:
            for _ in range(3):
                sierpinski(l / 2, n - 1)
                turtle.forward(l)
                turtle.left(120)
            
    turtle.penup()
    turtle.goto(-250, -225)
    turtle.pendown()
    sierpinski(500, n)

    turtle.save_as(f'sierpinski_{n}.svg')

