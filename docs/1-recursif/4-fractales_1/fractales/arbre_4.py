from svg_turtle import SvgTurtle
from random import randrange

turtle = SvgTurtle(700, 550)

def tronc(a: float):
    "Dessine un tronc de longueur a"
    # version aléatoire
    nb_pas = 10
    for _ in range(nb_pas):
        e = randrange(300, 367) / 1000
        angle = randrange(-500, 500) / 1000
        turtle.pensize(a * e)
        turtle.left(angle)
        turtle.forward(a / nb_pas)

def arbre(a: float, n: int):
    """Si n > 0:
        - Dessine un arbre ayant un tronc de longueur a,
        - et deux branches qui seront des arbres
            - de niveau n - 1,
            - de taille réduite d'un facteur `k = 0.75`
            - et orientées à 45°, de part et d'autre du tronc.
    La tortue retrouve sa position et son orientation initiale !
    """
    if n > 0:
        tronc(a)
        signe = +1
        for _ in range(2):
            signe = - signe
            k = randrange(500, 900) / 1000
            angle = randrange(20000, 70000) / 1000
            turtle.left(signe * angle)
            arbre(k * a, n - 1)
            turtle.right(signe * angle)
        turtle.penup()
        turtle.backward(a)  # retour au départ
        turtle.pendown()

turtle.speed(1)  # de 1 (lent) à 11 (rapide)
turtle.left(90)  # pour regarder vers le haut
turtle.penup()
turtle.backward(225)  # recule de 100
turtle.pendown()

arbre(150, 8)

turtle.save_as('arbre_4.svg')
