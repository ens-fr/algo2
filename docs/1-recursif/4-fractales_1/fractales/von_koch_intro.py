from svg_turtle import SvgTurtle
turtle = SvgTurtle(650, 300)

def koch(a: float, n: int):
    """Dessine la courbe de Koch, vers la gauche,
    sur une distance `a` à vol d'oiseau,
    la tortue est décalée à la fin de la courbe,
    elle conserve son orientation initiale.
    n : profondeur de récursion
    """
    if n == 0:
        turtle.forward(a)
    else:
        b = a / 3
        koch(b, n - 1)
        turtle.left(60)
        koch(b, n - 1)
        turtle.right(120)
        koch(b, n - 1)
        turtle.left(60)
        koch(b, n - 1)

turtle.speed(11)  # de 1 (lent) à 11 (rapide)
turtle.penup()
turtle.goto(-300, -90)
turtle.pendown()
b = 200; n = 6
turtle.color('red')
koch(b, n - 1)
turtle.color('black')
turtle.forward(50)
turtle.left(90)
turtle.circle(50, 60)
turtle.right(90)
turtle.backward(50)
#turtle.left(60)
turtle.color('blue')
koch(b, n - 1)
turtle.color('black')
turtle.forward(50)
turtle.left(90)
turtle.circle(50, -120)
turtle.right(90)
turtle.backward(50)
#turtle.right(120)
turtle.color('red')
koch(b, n - 1)
turtle.color('black')
turtle.forward(50)
turtle.left(90)
turtle.circle(50, 60)
turtle.right(90)
turtle.backward(50)
#turtle.left(600)
turtle.color('blue')
koch(b, n - 1)
turtle.save_as('von_koch_intro.svg')
