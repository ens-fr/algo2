from svg_turtle import SvgTurtle
turtle = SvgTurtle(700, 550)

def tronc(a: float):
    "Dessine un tronc de longueur a"
    # version basique
    turtle.pensize(a / 3)
    turtle.forward(a)

k = 0.75  # coefficient de réduction

def arbre(a: float, n: int):
    """Si n > 0:
        - Dessine un arbre ayant un tronc de longueur a,
        - et deux branches qui seront des arbres
            - de niveau n - 1,
            - de taille réduite d'un facteur `k = 0.75`
            - et orientées à 45°, de part et d'autre du tronc.
    La tortue retrouve sa position et son orientation initiale !
    """
    if n > 0:
        tronc(a)
        turtle.left(45)
        arbre(k * a, n - 1)
        turtle.right(90)
        arbre(k * a, n - 1)
        turtle.left(45)
        turtle.penup()
        turtle.backward(a)  # retour au départ
        turtle.pendown()

turtle.speed(1)  # de 1 (lent) à 11 (rapide)
turtle.left(90)  # pour regarder vers le haut
turtle.penup()
turtle.backward(225)  # recule de 100
turtle.pendown()

arbre(150, 8)

turtle.save_as('arbre_1.svg')
