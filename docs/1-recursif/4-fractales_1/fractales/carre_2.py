import drawSvg as draw

figure = draw.Drawing(500, 500, origin='center')

def square_2(x, y, a, n):
    for dx, dy in [(-1, -1), (+1, -1), (-1, +1), (+1, +1),]:
        figure.append(draw.Lines(
                x + dx * a/2, y + dy * a/2,
                x           , y + dy * a/2,
                x + dx * a/2, y,
            close=True, fill='black'))
        
    if n > 0:
        a /= 2
        square_2(x, y, a, n - 1)


a = 200
x, y = 0, 0
n = 4           # profondeur de récursion

square_2(x, y, a, n)

figure.saveSvg('carre_2.svg')
