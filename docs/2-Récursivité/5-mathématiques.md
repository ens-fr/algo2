# Mathématiques

## Triangle de Pascal

Chaque nombre est la somme du nombre au-dessus et au-dessus à gauche.

Construire un programme Python pour recréer cette liste de liste.

```pycon
[
    [1],
    [1, 1],
    [1, 2, 1],
    [1, 3, 3, 1],
    [1, 4, 6, 4, 1],
]
```

??? done "Solution"
    
    ```python
    def triangle_pascal(n):
        """Renvoie un triangle de pascal de n+1 lignes

        >>> for ligne in triangle_pascal(4): print(ligne)
        [1]
        [1, 1]
        [1, 2, 1]
        [1, 3, 3, 1]
        [1, 4, 6, 4, 1]

        """

        if n == 0:
            return [[1]]
        base = triangle_pascal(n-1)
        ligne = base[-1]    # la dernière ligne
        nouvelle_ligne = [1]
        for i in range(n-1):
            nouvelle_ligne.append(ligne[i] + ligne[i+1])
        nouvelle_ligne.append(1)
        base.append(nouvelle_ligne)
        return base
    ```


## Fonction d'Ackermann

Coder une version Python de la [Fonction d'Ackermann](https://fr.wikipedia.org/wiki/Fonction_d%27Ackermann)

??? done "Solution"
    À venir

## Récursions imbriquées

D'après John McCarthy :

$$
f_{91}(n)=
\begin{cases}
n-10, &\text{si } n > 100\\
f_{91}\left(f_{91}(n+11)\right), &\text{si } n \leqslant 100\\
\end{cases}
$$

1. Implémenter cette fonction en Python.
2. Donner un tableau de valeurs de $f_{91}(n)$, pour $n\in [\![0..100]\!]$.

??? done "Solution"
    ```python
    def f_91(n: int) -> int:
        if n > 100:
            return n - 10
        else:
            return f_91(f_91(n + 11))

    print([f_91(n) for n in range(101)])
    ```

    ```output
    [91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91, 91]
    ```

    On constate que la [fonction 91](https://fr.wikipedia.org/wiki/Fonction_91_de_McCarthy) est constante sur $[\![0..100]\!]$.

## Nb façons d'écrire un entier comme une somme 💥💥💥

On considère $f(n)$ : le nombre de façons d'écrire un entier $n>0$ comme somme d'entiers strictement positifs, sans tenir compte de l'ordre.

Par exemple, $5$ peut s'écrire de $f(5) = 7$ façons :

- $1+1+1+1+1$ ; la somme la plus longue,
- $2+2+1$,
- $1+3+1$,
- $2+3$,
- $5$ ; oui, une somme à un seul terme,
- $1+4$,
- $1+2+1+1$.

Écrire une fonction qui renvoie $f(n)$.

??? done "Solution"
    À vous !!!

## Quelques suites numériques utilisant l'auto-référence

💥💥💥 Le coin NSI + maths expertes 💥💥💥

[Douglas Hofstadter](https://fr.wikipedia.org/wiki/Douglas_Hofstadter) est l'auteur du livre [Gödel, Escher, Bach : Les Brins d'une Guirlande Éternelle](https://fr.wikipedia.org/wiki/G%C3%B6del,_Escher,_Bach_:_Les_Brins_d%27une_Guirlande_%C3%89ternelle).

On y trouve en particulier certaines suites étonnantes.

### 1- *Hofstadter Q-sequence*

Cette suite ressemble à celle de Fibonacci ou de Lucas, chaque terme est la somme de deux termes presque précédents.

La suite $a$ est définie par :

- $a_1 = a_2 = 1$,
- $a_n = a_{n-a_{n-1}} + a_{n-a_{n-2}}$, pour $n \geqslant 3$.

Personne n'a prouvé que cette suite est bien définie pour tout $n\in\mathbb N^*$.

On ne connait pas son taux de croissance.

> Écrire un code qui calcule les termes successifs en vérifiant que chacun est bien défini.

$a = (1, 1, 2, 3, 3, 4, 5, 5, 6, 6, 6, 8, 8, 8, 10, 9, \cdots)$ ; suite http://oeis.org/A005185

### 2- *Hofstadter Figure-Figure sequences*

Les suites *Hofstadter Figure-Figure* $R$ et $S$ sont des suites d'entiers complémentaires définies par :

- $R_1 = 1$, $S_1 = 2$.
- $R_n = R_{n - 1} + S_{n-1}$, pour $n > 1$.
- avec la suite $(S_{n})$ définie comme strictement croissante contenant tous les entiers absents de $(R_{n})$.

Les premiers termes sont :

- $R = (1, 3, 7, 12, 18, 26, 35, 45, 56, 69, 83, 98, 114, 131, 150, 170, 191, 213, 236, 260, \cdots)$ ; suite http://oeis.org/A005228
- $S = (2, 4, 5, 6, 8, 9, 10, 11, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24, 25, \cdots)$ ; suite http://oeis.org/A030124

> Implémenter les fonctions `R` et `S` en Python.

### 3- *Hofstadter-Conway $10,000 dollars sequence*

La suite $a$ est définie par :

- $a_{1} = a_{2} = 1$,
- $a_{n} = a_{a_{n-1}} + a_{n-a_{n-1}}$, pour $n \geqslant 3$.

$a = (1, 1, 2, 2, 3, 4, 4, 4, 5, 6, 7, 7, 8, 8, 8, 8, 9, 10, 11, 12, \cdots)$ ; suite http://oeis.org/A004001

1. *(Facile)* Calculer les premiers termes.
2. *(Variable)* Étudier le comportement asymptotique de $S(n) = \sum\limits_{i=1}^n a_i$. 💥💥
3. *(Vraiment très difficile)* [Vérifier si votre code est efficace](https://www.spoj.com/problems/HC10000/). 💥💥💥💥💥
