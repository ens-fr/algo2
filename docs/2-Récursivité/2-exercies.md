# Exercices concrets

## Nombre de chiffres

Écrire une version récursive d'une fonction qui renvoie le nombre de chiffres d'un entier strictement positif.

??? tip "Indice"
    Quel est le nombre de chiffres de $n$, par rapport à celui de $n$ divisé par $10$ ?

??? done "Solution"

    ```python
    def nb_chiffres(n: int) -> int:
        """Renvoie le nombre de chiffres de n.
        n écrit en base 10.

        >>> nb_chiffres(42)
        2
        >>> nb_chiffres(1337)
        4

        """
        if n < 10:
            return 1
        else:
            return 1 + nb_chiffres(n // 10)
    ```

    Par exemple, `nb_chiffres(1337) = 1 + nb_chiffres(133) = 1 + 1 + nb_chiffres(13) = 1 + 1 + 1 + nb_chiffres(1) = 1 + 1 + 1 + 1 = 4`

## Nombre de bits égaux à 1

Écrire une version récursive d'une fonction qui renvoie le nombre de bits égaux à $1$ d'un entier strictement positif.

??? tip "Indice"
    S'inspirer de l'exercice précédent.

??? done "Solution"
    ```python
    def nb_bits_à_1(n: int) -> int:
        """Renvoie le nombre de bits de n égaux à 1.

        Avec par exemples  7 = (111)_2 et 17 = (10001)_2

        >>> nb_bits_à_1(7)
        3

        >>> nb_bits_à_1(17)
        2

        """
        if n == 0:
            return 0
        else:
            bit_faible = n % 2
            return bit_faible + nb_bit_à_1(n // 2)
    ```

    > Remarque, il existe des opérateurs basiques pour obtenir le bit de poids faible, et la division par deux d'un entier.

    ```python
    def nb_bits_à_1(n: int) -> int:
        """Renvoie le nombre de bits de n égaux à 1.
        n écrit en binaire.

        >>> nb_bits_à_1(7)
        3

        >>> nb_bits_à_1(17)
        2

        """
        if n == 0:
            return 0
        else:
            bit_faible = n & 1
            return bit_faible + nb_bit_à_1(n >> 1)
    ```

    - `&` réalise un `et logique bit à bit`. Appliqué avec le masque `1`, on obtient le bit de poids faible.
    - `>>` réalise un décalage à droite de l'écriture binaire, avec perte des bits de poids faible. Décaler de 1 à droite revient à diviser par deux.

## Calcul de puissance

En partant du principe que :

- si $n$ est pair, alors $a^n = \left(a^{n/2}\right)^2$
- si $n$ est impair, alors $a^n = \left(a^{(n-1)/2}\right)^2×a$

> Exemples
>
> 1. $a^{2021} = (a^{1010})^2×a$
> 2. $a^{1010} = (a^{505})^2$
> 3. ...

Écrire une fonction récursive `puissance(a, n)` qui renvoie $a^n$.

??? tip "Indice"
    Penser au cas de base !

??? done "Solution"
    ```python
    def puissance(a, n: int):
        """Renvoie `a` à la puissance `n`.

        >>> puissance(13, 0)
        1
        >>> puissance(3, 5)
        243
        
        """
        if n == 0:
            # `a` à la puissance 0 est égal à 1
            return 1
        else:
            if n % 2 == 0:
                # n est pair
                return puissance(a, n//2) ** 2
            else:
                # n est impair
                return puissance(a, n//2) ** 2 * a
    ```

    Et une version un peu plus efficace, avec un code un peu factorisé.

    ```python
    def puissance(a, n: int):
        """Renvoie `a` à la puissance `n`.

        >>> puissance(13, 0)
        1
        >>> puissance(3, 5)
        243
        
        """
        if n == 0:
            return 1
        else:
            a_demi_n = puissance(a, n >> 1)
            carré = a_demi_n * a_demi_n
            if n & 1 == 0:
                # n est pair
                return carré
            else:
                # n est impair
                return carré * a
    ```

Compter à la main le nombre d'appels récursifs pour `puissance(7, 20)`.

??? done "Solution"
    - `puissance(7, 20)` appelle `puissance(7, 10)`
    - `puissance(7, 10)` appelle `puissance(7, 5)`
    - `puissance(7, 5)` appelle `puissance(7, 2)`
    - `puissance(7, 2)` appelle `puissance(7, 1)`
    - `puissance(7, 1)` appelle `puissance(7, 0)`
    - Il y a 5 appels récursifs.





