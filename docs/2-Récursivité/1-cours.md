# Cours

!!! cite inline end "Une tradition culturelle"
    Nous les hackers nous [...] avions aussi une tradition d'acronymes récursifs qui consiste à dire que le programme qu'on crée est similaire à un programme existant.
    
    *Richard Stallman*

## Exemples

!!! info "Sigles récursifs"

    - **GNU** : *GNU's Not UNIX* (GNU n'est pas UNIX)
    - **LAME** : *Lame Ain't an MP3 Encoder* (Lame n'est pas un encodeur mp3.)
    - **PHP** : *PHP: Hypertext Preprocessor*. (Historiquement, ce sigle récursif était l'abréviation de Personal Home Page ; en 2008, le sigle récursif est le sens officiel de PHP)
    - **HURD** : *Hird of Unix-Replacing Daemons* et **HIRD** : *Hurd of Interfaces Representing Depth* ; exemple de paire de sigles mutuellement récursifs.

On trouve aussi des images associées à cette notion.

Dans les Arts

![cette image n'est pas sous licence libre](assets/tumblr_l9bz3qDYWF1qdmjkto1_1280.jpg)
> *Les Deux Mystères*, René Magritte

Dans la nature, on trouve différents objets qui font preuve d'auto-similarité. On peut aussi les construire artificiellement.

Dans la nature

![](assets/Fractal_Broccoli.jpg)

 En informatique

![](assets/Bransleys_fern.png)

## Définitions

(D'après [Wikipédia](https://fr.wikipedia.org/wiki/R%C3%A9cursivit%C3%A9))
La récursivité est une démarche qui fait référence à l'objet même de la démarche à un moment du processus. En d'autres termes, c'est une démarche dont la description mène à la répétition d'une même règle. Ainsi, les cas suivants constituent des cas concrets de récursivité :

1. Décrire un processus dépendant de données en faisant appel à ce même processus sur d'autres données plus « simples » ;
2. Montrer une image contenant des images similaires ;
3. Écrire un algorithme qui s'invoque lui-même ;
4. Définir une structure à partir de l'une au moins de ses sous-structures.

En NSI, nous abordons ces différents aspects.

1. Le concept «Diviser pour mieux régner » donne par exemple le principe de recherche par dichotomie dans un tableau trié.
2. Les [fractales](https://fr.wikipedia.org/wiki/Fractale), par exemple.
3. Nous allons voir de nombreux exemples de fonctions récursives !
4. Les structures arborescentes, arbres et graphes, par exemple.

## Fonctions récursives

Une **fonction récursive** est une fonction qui s'appelle elle-même. (Ou bien qui fait partie d'un ensemble de fonctions qui s'appellent mutuellement).

### Exemple, somme des premiers entiers

$$S_n = 1 + 2 + 3 + \cdots + (n-1) + n$$

On a un définition récursive :

- $S_0 = 0$, et
- $S_{n} = S_{n-1} + n$

Par exemple,

- $S_5 = 1 + 2 + 3 + 4 + 5 = 15$.
- $S_4 = 1 + 2 + 3 + 4 = 10$.
- $S_5 = S_4 + 5$

#### Version itérative

```python
def somme_premiers_entiers(n):
    """Renvoie la somme : 0 + 1 + ... + n

    >>> somme_premiers_entiers(5)
    15

    """
    somme = 0
    for i in range(1, n+1):
        somme += i
    return somme
```

#### Version récursive

```python
def somme_premiers_entiers(n):
    """Renvoie la somme : 0 + 1 + ... + n

    >>> somme_premiers_entiers(5)
    15

    """
    if n == 0:
        return 0
    else:
        return somme_premiers_entiers(n-1) + n
```

- On ne voit pas de boucle, mais c'est presque équivalent, il y a une succession d'appels récursifs.
- Les appels récursifs sont stockés dans une pile d'appels.
- En pratique, il y aura une boucle en arrière plan, et de la mémoire utilisée pour stocker les paramètres d'appels en attendant la dernière réponse...

!!! warning "Limite avec Python"
    :warning: **Attention** :warning:, par défaut, Python limite à $1000$ la profondeur des appels récursifs.
    
    Cela peut se modifier. Par exemple à $100\,000$.

    ```python
    import sys
    sys.setrecursionlimit(10**5)
    ```

### Différents types de fonctions récursives

#### Appels multiples

Voici, par exemple, une version naïve pour calculer un terme de la suite de Fibonacci.

!!! info "Suite de Fibonacci"
    Il s'agit de  $[0, 1, 1, 2, 3, 5, 8, 13, 21, \cdots]$

    On commence avec $[0, 1]$, puis chaque nouveau terme est la somme des deux précédents.

```python
def fibonacci(n):
    """Renvoie le terme d'indice n de la suite

    >>> fibonacci(6)
    8

    >>> fibonacci(0)
    0

    """
    if n < 2:
        return n
    else:
        return fibonacci(n-1) + fibonacci(n-2)
```

Cette version est dite naïve, en effet l'appel `fibonacci(5)` est effectué de nombreuses fois pour l'appel `fibonacci(8)` (par exemple) et le résultat n'est pas stocké, donc recalculé à chaque fois... On utilise la **mémoïsation** pour améliorer cela, comme ci-dessous.

```python
fib_dico = {0: 0, 1: 1} # valeurs initiales

def fibonacci(n):
    """Renvoie le terme d'indice n de la suite

    >>> fibonacci(6)
    8

    >>> fibonacci(0)
    0

    """
    if n not in fib_dico:
        fib_dico[n] = fibonacci(n-1) + fibonacci(n-2)

    return fib_dico[n]
```

#### Appels croisés

```python
def fonction_A(x):
    ...
    ...
    ...fonction_B(...x...)
    ...
    return ...

def fonction_B(x):
    ...
    ...
    ...fonction_A(...x...)
    ...
    return ...
```

La `fonction_A` utilise la `fonction_B` qui utilise elle-même la `fonction_A`, *a priori* avec un paramètre qui dépend du ou des paramètres donnés précédemment...

:warning: Faire des appels croisés est **légal**, cependant on veillera que cela fasse progresser le « calcul », donc sans rentrer dans une boucle infinie. On remarquera que ce principe est général. Le contre-exemple simple suivant boucle à l'infini pour toute entrée `n` supérieure à `0`.

```python
def bizarre(n):
    if n == 0:
        return 9
    else:
        return bizarre(n + 1)
```
